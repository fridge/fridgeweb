<?php
header("Content-type: text/xml");
$cookie_check_override = true;
$no_session = true;
require_once("includes.php");
require_once("database_functions.php");
require_once("policy.php");

$NONCE_CHARS = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
$NONCE_LENGTH = 20;
$MAX_AGE = 600; //Maximum age of timestamp from client, in seconds. Note that server and client clocks may be a bit out.

//Attempt to authenticate the given (real or interfridge) user
//Return the user's balance if successfully authenticated, or else a string error message
function authenticate_user($params, $name, $hmac, &$password_md5, &$is_grad, &$is_interfridge) {
	$result = DBQuery("SELECT password_md5, isgrad, isinterfridge, balance * 100 AS balance_i FROM users WHERE username='" . pg_escape_string($name) . "' AND enabled");
	if (pg_num_rows($result) != 1) {
		//No such (enabled, interfridge if appropriate) user
		return "No such user $name.";
	}
	$row = pg_fetch_assoc($result);
	if ($hmac != sign($row['password_md5'], $params)) {
		return 'Invalid HMAC.';
	}
	
	//Successfully authenticated
	$password_md5 = $row['password_md5'];
	$is_grad = ($row['isgrad'] == 't');
	$is_interfridge = ($row['isinterfridge'] == 't');
	return (int) $row['balance_i'];
}

function error($code, $message) {
	return array('faultCode' => $code, 'faultString' => $message);
}

function make_nonce() {
	global $NONCE_CHARS, $NONCE_LENGTH;
	$nonce = '';
	for ($i = 0; $i < $NONCE_LENGTH; ++$i) {
		$nonce .= substr($NONCE_CHARS, rand(0, strlen($NONCE_CHARS) - 1), 1);
	}
	return $nonce;
}

function items_to_string($items) {
	$items_string = '';
	foreach ($items as $item) {
		$items_string .= $item['code'] . "," . $item['quantity'] . ",";
	}
	$items_string = substr($items_string, 0, -1);
	return $items_string;
}

//Utility method used by sign, to turn arbitrary data into a string from which to generate the signature
function stringify($data) {
	if (is_array($data)) { //This covers both the array and the map cases, as they are the same in PHP
		//Join values sorted by key
		ksort($data);
		$string = '';
		$first = true;
		foreach ($data as $key => $value) {
			if ($first) {
				$first = false;
			}
			else {
				$string .= ",";
			}
			$string .= stringify($value);
		}
		return $string;
	}
	else {
		return (string) $data;
	}
}

//Given an MD5-ed password and an array of parameters, generate the appropriate HMAC for a method call or for checking the signature on a method result
function sign($password_md5, $data) {
	$string = stringify($data);
	return hash_hmac('md5', $string, $password_md5);
}

//Get price (in cents) for a given product, including markup and graduate discount if appropriate
function get_price_for_code($code, $is_grad) {
	$query_result = DBQuery("SELECT cost, markup FROM product WHERE product_code='" . $code . "'");
	if (pg_num_rows($query_result) != 1) {
		//TODO: Error and return
		return "No such product code $code.";
	}
	$row = pg_fetch_assoc($query_result);
	//Calculate the price from cost + markup, in cents, rounded to nearest cent
	$markup = $row['markup'];
	if (!$is_grad) {
		$query_result = DBQuery("SELECT value FROM numerical_variables WHERE variable='graduate_discount'");
		$var_row = pg_fetch_assoc($query_result);
		$graduate_discount = (double) $var_row['value'];
		$markup += $graduate_discount;
	}
	return (int) (($row['cost'] * ($markup / 100 + 1) * 100) + 0.5);
}

//Helper function to encode an XMLRPC request, make the HTTP request and decode the result
function xmlrpc_call($endpoint, $method, $arguments) {
	$request = xmlrpc_encode_request($method, $arguments);
	$context = stream_context_create(array('http' => array(
		'method' => 'POST',
		'header' => 'Content-Type: text/xml',
		'content' => $request
	)));
	$file = file_get_contents($endpoint, false, $context);
	$response = xmlrpc_decode($file);
	return $response;
}

//Wrappers to call remote methods via XMLRPC. These all return null on failure.
function call_generate_nonce($endpoint, $username, $password) {
	//Prepare arguments
	$timestamp = time();
	$cnonce = make_nonce();
	$request_hmac = sign(md5($password), array($cnonce, $timestamp, $username));
	
	//Actually make XMLRPC call
	$result = xmlrpc_call($endpoint, 'generate_nonce', array($cnonce, $timestamp, $username, $request_hmac));
	if (xmlrpc_is_fault($result)) {
		return null;
	}
	
	//Check HMAC on response
	$snonce = $result['nonce'];
	if (sign(md5($password), array($snonce, $cnonce)) != $result['hmac']) {
		return null;
	}
	
	return $snonce;
}

function call_transfer($endpoint, $snonce, $from_user, $to_user, $amount, $password) {
	//Calculate HMAC
	$request_hmac = sign(md5($password), array($snonce, $from_user, $to_user, $amount));
	
	//Actually make XMLRPC call
	$result = xmlrpc_call($endpoint, 'transfer', array($snonce, $from_user, $to_user, $amount, $request_hmac));
	if (xmlrpc_is_fault($result)) {
		return null;
	}
	
	//Check HMAC on response
	$balance = $result['balance'];
	if (sign(md5($password), array($snonce, $balance)) != $result['hmac']) {
		return null;
	}
	
	return $balance;
}

function call_purchase($endpoint, $snonce, $user, $items, $password) {
	//Calculate HMAC
	$request_hmac = sign(md5($password), array($snonce, $user, $items));
	
	//Actually make XMLRPC call
	$result = xmlrpc_call($endpoint, 'purchase', array($snonce, $user, $items, $request_hmac));
	if (xmlrpc_is_fault($result)) {
		return null;
	}
	
	//Check HMAC on response
	$balance = $result['balance'];
	$order_total = $result['order_total'];
	if (sign(md5($password), array($snonce, $balance, $order_total)) != $result['hmac']) {
		return null;
	}
	
	return $result;
}

function call_topup($endpoint, $snonce, $user, $amount, $password) {
	//Calculate HMAC
	$request_hmac = sign(md5($password), array($snonce, $user, $amount));
	
	//Actually make XMLRPC call
	$result = xmlrpc_call($endpoint, 'topup', array($snonce, $user, $amount, $request_hmac));
	if (xmlrpc_is_fault($result)) {
		return null;
	}
	
	//Check HMAC on response
	$balance = $result['balance'];
	if (sign(md5($password), array($snonce, $balance)) != $result['hmac']) {
		return null;
	}
	
	return $result;
}

//Methods exposed via XMLRPC
function generate_nonce($method_name, $params, $app_data) {
	global $MAX_AGE;
	list($cnonce, $timestamp, $username, $request_hmac) = $params;
	
	//Get rid of old nonces
	DBQuery("DELETE FROM nonces WHERE created_at < NOW() - INTERVAL '10 minutes'");
	
	//Check the timestamp
	if (time() - $timestamp > $MAX_AGE) {
		return error(1, 'Timestamp too old.');
	}
	//Check client nonce (namely that it is not already associated with a server nonce)
	$result = pg_fetch_array(DBQuery("SELECT COUNT(*) FROM nonces WHERE cnonce='" . pg_escape_string($cnonce) . "'", 0));
	if ($result[0] != 0) {
		return error(2, 'Invalid client nonce.');
	}
	//Check the HMAC from the client
	$authenticated_balance = authenticate_user(array($cnonce, $timestamp, $username), $username, $request_hmac, $password_md5, $is_grad, $is_interfridge);
	if (!is_int($authenticated_balance)) {
		//Authentication failed.
		return error(3, "Error authenticating for nonce: $authenticated_balance");
	}
	
	//Generate a nonce
	$snonce = make_nonce();
	DBQuery("INSERT INTO nonces (snonce, cnonce) VALUES('" . pg_escape_string($snonce) . "', '" . pg_escape_string($cnonce) . "')");
	
	//Calculate the HMAC
	$response_hmac = sign($password_md5, array($snonce, $cnonce));
	return array('nonce' => $snonce, 'hmac' => $response_hmac);
}

//Transfer an amount of money from one user to another, returning the new balance of the user who sent the money
function transfer($method_name, $params, $app_data) {
	global $LOCAL_FRIDGE_NAME;
	list($snonce, $from_user, $to_user, $amount, $user_hmac) = $params;
	
	if (!is_int($amount)) {
		return error(4, 'Wrong type for amount.');
	}
	
	//Check that nonce is valid
	$result = pg_fetch_array(DBQuery("SELECT COUNT(*) FROM nonces WHERE snonce='" . pg_escape_string($snonce) . "'", 0));
	if ($result[0] != 1) {
		return error(5, 'Invalid nonce.');
	}
	//Nonce is valid, delete it to prevent replays
	DBQuery("DELETE FROM nonces WHERE snonce='" . pg_escape_string($nonce) . "'");
	
	//Authenticate the user by the user_hmac
	$user_authenticated_balance = authenticate_user(array($snonce, $from_user, $to_user, $amount), $from_user, $user_hmac, $password_md5, $is_grad, $is_interfridge);
	if (!is_int($user_authenticated_balance)) {
		//Authentication failed.
		return error(6, "Error authenticating user: $user_authenticated_balance");
	}
	
	//Everything is authenticated fine, now check whether local policy allows the transfer
	$policy = policy_transfer($from_user, $to_user, $amount, $user_authenticated_balance, $is_interfridge, $is_grad);
	if ($policy !== null) {
		return error(7, $policy);
	}
	
	//Parse the destination user
	if ($at = strrpos($to_user, '@')) {
		//Remote user
		$remote_to_user = substr($to_user, 0, $at);
		$local_to_user = $to_fridge = substr($to_user, $at + 1);
	}
	else {
		//Local user
		$local_to_user = $to_user;
	}
	
	//Make the transfer
	if ($amount > 0) {
		//Make the remote transfer, if necessary
		if (isset($to_fridge)) {
			//Find the remote fridge
			$result = DBQuery("SELECT fridgeserver_endpoint, interfridge_password FROM users WHERE username='" . pg_escape_string($to_fridge) . "' AND enabled AND isinterfridge");
			if (pg_num_rows($result) == 0) {
				//No such fridge
				return error(10, "No such fridge $to_fridge.");
			}
			$row = pg_fetch_assoc($result);
			$to_fridge_endpoint = $row['fridgeserver_endpoint'];
			$to_fridge_password = $row['interfridge_password'];
			
			if (($remote_snonce = call_generate_nonce($to_fridge_endpoint, $LOCAL_FRIDGE_NAME, $to_fridge_password)) === null) {
				return error(11, "Error generating remote nonce ($to_fridge_endpoint, $LOCAL_FRIDGE_NAME, ***)."); //TODO: Make this less verbose
			}
			if (call_transfer($to_fridge_endpoint, $remote_snonce, $LOCAL_FRIDGE_NAME, $remote_to_user, $amount, $to_fridge_password) === null) {
				return error(12, "Error making remote transfer ($to_fridge_endpoint, $remote_snonce, $LOCAL_FRIDGE_NAME, $remote_to_user, $amount, ***)."); //TODO: Make this less verbose
			}
			//It worked
		}
		
		//The remote transfer succeeded, so make the local transfer
		$transfer_errors = transferCredit($amount / 100.0, $from_user, $local_to_user);
	}
	else {
		//Amount is 0, which is used for authenticating a user without making a purchase. Do not actually record a transfer.
		$transfer_errors = array();
	}
	if (count($transfer_errors) != 0) {
		//Errors attempting to transfer
		$errors = implode(', ', $transfer_errors);
		return error(9, "Errors transferring: $errors");
	}
	//Success! Return the user's new balance after making the transfer. We also need to calculate the HMAC for this.
	$new_balance = $user_authenticated_balance - $amount;
	$fridge_response_hmac = sign($password_md5, array($snonce, $new_balance));
	return array('balance' => $new_balance, 'hmac' => $fridge_response_hmac);
}

//Get a list of items currently in stock
function get_stock($method_name, $params, $app_data) {
	if (count($params) == 0) {
		//If no user given, default to showing undergrad prices
		$is_grad = false;
	}
	else {
		list($user) = $params;
		//Check whether the user is a graduate, so as to show appropriate prices
		$user_result = DBQuery("SELECT isgrad FROM users WHERE username='" . pg_escape_string($user) . "' AND enabled");
		if (pg_num_rows($user_result) != 1) {
			//No such (enabled, interfridge if appropriate) user
			return error(16, "No such user $user.");
		}
		$user_row = pg_fetch_assoc($user_result);
		$is_grad = ($user_row['isgrad'] == 't');
	}
	
	if ($is_grad) {
		$graduate_discount = 0.0;
	}
	else {
		$query_result = DBQuery("SELECT value FROM numerical_variables WHERE variable='graduate_discount'");
		$var_row = pg_fetch_assoc($query_result);
		$graduate_discount = (double) $var_row['value'];
	}
	
	$query_result = DBQuery("SELECT * FROM product INNER JOIN category USING (category_id) WHERE enabled ORDER BY product_code");
	$stock = array();
	while ($row = pg_fetch_assoc($query_result)) {
		//Calculate the price from cost + markup, in cents, rounded to nearest cent
		$markup = $row['markup'] + $graduate_discount;
		$price = (int) (($row['cost'] * ($markup / 100 + 1) * 100) + 0.5);
		$stock[] = array('product_code' => $row['product_code'], 'description' => $row['description'], 'in_stock' => (int) $row['in_stock'], 'price' => $price, 'category' => $row['title'], 'category_order' => (int) $row['display_sequence']);
	}
	return $stock;
}

//Get a list of peered fridges
function get_fridges($method_name, $params, $app_data) {
	$query_result = DBQuery("SELECT username, fridgeserver_endpoint FROM users WHERE isinterfridge ORDER BY username");
	$fridges = array();
	while ($row = pg_fetch_assoc($query_result)) {
		$fridges[$row['username']] = $row['fridgeserver_endpoint'];
	}
	//FIXME: This returns an array rather than a struct when there are no peered fridges
	return $fridges;
}

function get_fridge_name($method_name, $params, $app_data) {
	global $LOCAL_FRIDGE_NAME;
	return $LOCAL_FRIDGE_NAME;
}

function get_messages($method_name, $params, $app_data) {
	$query_result = pg_fetch_assoc(DBQuery("SELECT COUNT(username) AS num_served FROM user_credit_log WHERE transaction_type='PURCHASE'"));
	$num_served = (int) $query_result['num_served'];
	
	$query_result = DBQuery("SELECT real_name FROM users WHERE isadmin ORDER BY real_name");
	$admins = array();
	while ($row = pg_fetch_assoc($query_result)) {
		$admins[] = $row['real_name'];
	}
	
	$query_result = DBQuery("SELECT username FROM users WHERE NOT isadmin AND enabled AND NOT isinterfridge AND balance < 0 ORDER BY balance LIMIT 5");
	$naughty_people = array();
	while ($row = pg_fetch_assoc($query_result)) {
		$naughty_people[] = $row['username'];
	}
	
	return array('num_served' => $num_served, 'signup_info' => "To use fridge, you'll need a DrinksCorp prepay account. To get one, talk to one of the following people:\n" . implode(', ', $admins), 'naughty_people' => $naughty_people);
}

//Get some basic information about the given user
function get_user_info($method_name, $params, $app_data) {
	list($user) = $params;
	
	//Look the user up in the database
	$user_result = DBQuery("SELECT real_name, isadmin, email_address FROM users WHERE username='" . pg_escape_string($user) . "' AND enabled");
	if (pg_num_rows($user_result) != 1) {
		//No such (enabled, interfridge if appropriate) user
		return error(16, "No such user $user.");
	}
	$user_row = pg_fetch_assoc($user_result);
	return array('real_name' => $user_row['real_name'], 'isadmin' => $user_row['isadmin'] == 't', 'avatar' => "http://www.gravatar.com/avatar/" . md5(strtolower($user_row['email_address'])) . "?d=identicon");
}

function get_user_log($method_name, $params, $app_data) {
	list($snonce, $user, $user_hmac) = $params;
	
	//Check that nonce is valid
	$result = pg_fetch_array(DBQuery("SELECT COUNT(*) FROM nonces WHERE snonce='" . pg_escape_string($snonce) . "'", 0));
	if ($result[0] != 1) {
		return error(5, 'Invalid nonce.');
	}
	//Nonce is valid, delete it to prevent replays
	DBQuery("DELETE FROM nonces WHERE snonce='" . pg_escape_string($nonce) . "'");
	
	//Authenticate the user by the user_hmac
	$user_authenticated_balance = authenticate_user(array($snonce, $user), $user, $user_hmac, $password_md5, $is_grad, $is_interfridge);
	if (!is_int($user_authenticated_balance)) {
		//Authentication failed.
		return error(6, "Error authenticating user: $user_authenticated_balance");
	}
	
	$result = DBQuery("SELECT * FROM user_credit_log WHERE username='" . pg_escape_string($user) . "' AND transaction_type <> 'PURCHASE' ORDER BY date_time DESC");
	$transactions = array();
	while ($row = pg_fetch_array($result)) {
		$date_time = $row['date_time'];
		$amount = (int) ($row['amount'] * 100);
		$sender = $row['transfer_received_from'];
		$recipient = $row['transfer_sent_to'];
		$type = $row['transaction_type'];
		$id = $date_time;
		
		if ($type == 'CREDIT') {
			$transaction = array('date_time' => $date_time, 'amount' => $amount, 'type' => $type, 'id' => $id);
		}
		else if ($type == 'XFER-OUT') {
			$transaction = array('date_time' => $date_time, 'amount' => $amount, 'type' => $type, 'recipient' => $recipient, 'id' => $id);
		}
		else if ($type == 'XFER-IN') {
			$transaction = array('date_time' => $date_time, 'amount' => $amount, 'type' => $type, 'sender' => $sender, 'id' => $id);
		}
		$transactions[] = $transaction;
	}
	$result = DBQuery("SELECT * FROM purchases WHERE username='" . pg_escape_string($user) . "' ORDER BY date_time DESC");
	while ($row = pg_fetch_array($result)) {
		$date_time = $row['date_time'];
		$amount = (int) ($row['amount'] * -100);
		$type = 'PURCHASE';
		$product_code = $row['product_code'];
		$purchase_quantity = (int) $row['purchase_quantity'];
		$id = "{$row['date_time']}{$row['product_code']}";
		
		$transactions[] = array('date_time' => $date_time, 'amount' => $amount, 'type' => $type, 'product_code' => $product_code, 'purchase_quantity' => $purchase_quantity, 'id' => $id);
	}
	$fridge_response_hmac = sign($password_md5, array($snonce, $transactions));
	return array('transactions' => $transactions, 'hmac' => $fridge_response_hmac);
}

function purchase($method_name, $params, $app_data) {
	list($snonce, $user, $items, $user_hmac) = $params;
	
	//Check that nonce is valid TODO: Move this check / delete nonce into a function
	$result = pg_fetch_array(DBQuery("SELECT COUNT(*) FROM nonces WHERE snonce='" . pg_escape_string($snonce) . "'", 0));
	if ($result[0] != 1) {
		return error(5, 'Invalid nonce.');
	}
	//Nonce is valid, delete it to prevent replays
	DBQuery("DELETE FROM nonces WHERE snonce='" . pg_escape_string($nonce) . "'");
	
	//Authenticate the user by the user_hmac
	$user_authenticated_balance = authenticate_user(array($snonce, $user, $items), $user, $user_hmac, $password_md5, $is_grad, $is_interfridge);
	if (!is_int($user_authenticated_balance)) {
		//Authentication failed.
		return error(6, "Error authenticating user: $user_authenticated_balance");
	}
	
	//Check whether local policy allows the purchase
	$policy = policy_purchase($user, $user_authenticated_balance, $is_interfridge, $is_grad);
	if ($policy !== null) {
		return error(7, $policy);
	}
	
	//All good, record the purchase
	DBQuery("BEGIN");
	$order_total = 0;
	//Rows of order
	foreach ($items as $order_line) {
		$code = $order_line['code'];
		$quantity = (int) $order_line['quantity'];
		if ($quantity == 0) {
			continue; //Ignore purchases of nothing
		}
		$price = get_price_for_code($code, $is_grad);
		if (!is_int($price)) {
			DBQuery("ROLLBACK");
			return error(14, $price);
		}
		$row_price = $quantity * $price / 100.0;
		if (DBQuery("UPDATE product SET in_stock=in_stock - $quantity WHERE product_code='" . pg_escape_string($code) . "'") === false) {
			DBQuery("ROLLBACK");
			return error(15, "Error updating stock level for $code.");
		}
		if (DBQuery("INSERT INTO purchases (username, product_code, date_time, purchase_quantity, amount, surplus) VALUES('" . pg_escape_string($user) . "', '" . pg_escape_string($code) . "', current_timestamp, $quantity, $row_price, $quantity*surplus_for_product('" . pg_escape_string($code) . "', '" . pg_escape_string($user) . "'))") === false) {
			DBQuery("ROLLBACK");
			return error(15, "Error creating purchase transaction for $code.");
		}
		$order_total += $quantity * $price;
	}
	
	//Debit user for purchase amount
	if (DBQuery("UPDATE users SET balance=balance - $order_total / 100.0 WHERE username='" . pg_escape_string($user) . "'") === false) {
		DBQuery("ROLLBACK");
		return error(15, "Error updating user balance for purchase.");
	}
	if (DBQuery("INSERT INTO user_credit_log (username, date_time, amount, transaction_type, transfer_received_from, transfer_sent_to) VALUES ('" . pg_escape_string($user) . "', current_timestamp, $order_total / -100.0, 'PURCHASE', NULL, NULL)") === false) {
		DBQuery("ROLLBACK");
		return error(15, "Error creating user credit transaction for purchase.");
	}
	
	//Get new balance
	$balance_row = pg_fetch_assoc(DBQuery("SELECT balance * 100 AS balance_i FROM users WHERE username='" . pg_escape_string($user) . "'"));
	$new_balance = (int) $balance_row['balance_i'];
	DBQuery("COMMIT");
	
	$fridge_response_hmac = sign($password_md5, array($snonce, $new_balance, $order_total));
	return array('balance' => $new_balance, 'order_total' => $order_total, 'hmac' => $fridge_response_hmac);
}

function topup($method_name, $params, $app_data) {
	list($snonce, $user, $amount, $user_hmac) = $params;
	
	if (!is_int($amount)) {
		return error(4, 'Wrong type for amount.');
	}
	
	//Check that nonce is valid TODO: Move this check / delete nonce into a function
	$result = pg_fetch_array(DBQuery("SELECT COUNT(*) FROM nonces WHERE snonce='" . pg_escape_string($snonce) . "'", 0));
	if ($result[0] != 1) {
		return error(5, 'Invalid nonce.');
	}
	//Nonce is valid, delete it to prevent replays
	DBQuery("DELETE FROM nonces WHERE snonce='" . pg_escape_string($nonce) . "'");
	
	//Authenticate the user by the user_hmac
	$user_authenticated_balance = authenticate_user(array($snonce, $user, $amount), $user, $user_hmac, $password_md5, $is_grad, $is_interfridge);
	if (!is_int($user_authenticated_balance)) {
		//Authentication failed.
		return error(6, "Error authenticating user: $user_authenticated_balance");
	}
	
	//Check whether local policy allows the topup
	$policy = policy_topup($user, $amount, $user_authenticated_balance, $is_interfridge, $is_grad);
	if ($policy !== null) {
		return error(7, $policy);
	}
	
	//All good, record the topup
	DBQuery("BEGIN");
	
	//Credit the user
	if (DBQuery("UPDATE users SET balance=balance + $amount / 100.0 WHERE username='" . pg_escape_string($user) . "'") === false) {
		DBQuery("ROLLBACK");
		return error(15, "Error updating user balance for topup.");
	}
	if (DBQuery("INSERT INTO user_credit_log (username, date_time, amount, transaction_type, transfer_received_from, transfer_sent_to) VALUES ('" . pg_escape_string($user) . "', current_timestamp, $amount / 100.0, 'CREDIT', NULL, NULL)") === false) {
		DBQuery("ROLLBACK");
		return error(15, "Error creating user credit transaction for topup.");
	}
	//Add to the drawer balance
	if (DBQuery("UPDATE numerical_variables SET value=value + $amount / 100.0 WHERE variable='drawer_balance'") === false) {
		DBQuery("ROLLBACK");
		return error(16, "Error updating drawer balance for topup.");
	}
	
	//Get new balance
	$balance_row = pg_fetch_assoc(DBQuery("SELECT balance * 100 AS balance_i FROM users WHERE username='" . pg_escape_string($user) . "'"));
	$new_balance = (int) $balance_row['balance_i'];
	DBQuery("COMMIT");
	
	$fridge_response_hmac = sign($password_md5, array($snonce, $new_balance));
	return array('balance' => $new_balance, 'hmac' => $fridge_response_hmac);
}

function remote_purchase($method_name, $params, $app_data) {
	global $LOCAL_FRIDGE_NAME;
	list($snonce, $purchase_fridge, $user, $items, $user_hmac) = $params;
	
	//Check that nonce is valid TODO: Move this check / delete nonce into a function
	$result = pg_fetch_array(DBQuery("SELECT COUNT(*) FROM nonces WHERE snonce='" . pg_escape_string($snonce) . "'", 0));
	if ($result[0] != 1) {
		return error(5, 'Invalid nonce.');
	}
	//Nonce is valid, delete it to prevent replays
	DBQuery("DELETE FROM nonces WHERE snonce='" . pg_escape_string($nonce) . "'");
	
	//Authenticate the user by the user_hmac
	$user_authenticated_balance = authenticate_user(array($snonce, $purchase_fridge, $user, $items), $user, $user_hmac, $password_md5, $is_grad, $is_interfridge);
	if (!is_int($user_authenticated_balance)) {
		//Authentication failed.
		return error(6, "Error authenticating user: $user_authenticated_balance");
	}
	
	//Check whether local policy allows the remote purchase
	$policy = policy_remote_purchase($purchase_fridge, $user, $user_authenticated_balance, $is_interfridge, $is_grad);
	if ($policy !== null) {
		return error(7, $policy);
	}
	
	//All good, call the purchase method on the remote fridge
	//Find the remote fridge
	$result = DBQuery("SELECT fridgeserver_endpoint, interfridge_password FROM users WHERE username='" . pg_escape_string($purchase_fridge) . "' AND enabled AND isinterfridge");
	if (pg_num_rows($result) == 0) {
		//No such fridge
		return error(10, "No such fridge $purchase_fridge.");
	}
	$row = pg_fetch_assoc($result);
	$purchase_fridge_endpoint = $row['fridgeserver_endpoint'];
	$purchase_fridge_password = $row['interfridge_password'];
	
	if (($remote_snonce = call_generate_nonce($purchase_fridge_endpoint, $LOCAL_FRIDGE_NAME, $purchase_fridge_password)) === null) {
		return error(11, "Error generating remote nonce ($purchase_fridge_endpoint, $LOCAL_FRIDGE_NAME, ***)."); //TODO: Make this less verbose
	}
	if (($remote_purchase_result = call_purchase($purchase_fridge_endpoint, $remote_snonce, $LOCAL_FRIDGE_NAME, $items, $purchase_fridge_password)) === null) {
		return error(12, "Error making remote purchase ($purchase_fridge_endpoint, $remote_snonce, $LOCAL_FRIDGE_NAME, $items, ***)."); //TODO: Make this less verbose
	}
	$order_total = $remote_purchase_result['order_total'];
	if (!is_int($order_total)) {
		//Bad remote fridge
		return error(12, 'Wrong type for order total returned by remote purchase.');
	}
	
	//Transfer amount from local user to fridge
	if ($order_total >= 0) {
		$transfer_errors = transferCredit($order_total / 100.0, $user, $purchase_fridge);
	}
	else {
		//Yes, the order total could be negative, if the user purchases a negative quantity of something to correct a mistake.
		$transfer_errors = transferCredit(-$order_total / 100.0, $purchase_fridge, $user);
	}
	if (count($transfer_errors) != 0) {
		//Errors attempting to transfer
		$errors = implode(', ', $transfer_errors);
		return error(17, "Remote purchase made, but errors transferring $order_total from $user to $purchase_fridge: $errors");
	}
	
	//Get new balance
	$balance_row = pg_fetch_assoc(DBQuery("SELECT balance * 100 AS balance_i FROM users WHERE username='" . pg_escape_string($user) . "'"));
	$new_balance = (int) $balance_row['balance_i'];
	
	$fridge_response_hmac = sign($password_md5, array($snonce, $new_balance, $order_total));
	return array('balance' => $new_balance, 'order_total' => $order_total, 'hmac' => $fridge_response_hmac);
}

function remote_topup($method_name, $params, $app_data) {
	global $LOCAL_FRIDGE_NAME;
	list($snonce, $purchase_fridge, $user, $amount, $user_hmac) = $params;
	
	//Check that nonce is valid TODO: Move this check / delete nonce into a function
	$result = pg_fetch_array(DBQuery("SELECT COUNT(*) FROM nonces WHERE snonce='" . pg_escape_string($snonce) . "'", 0));
	if ($result[0] != 1) {
		return error(5, 'Invalid nonce.');
	}
	//Nonce is valid, delete it to prevent replays
	DBQuery("DELETE FROM nonces WHERE snonce='" . pg_escape_string($nonce) . "'");
	
	//Authenticate the user by the user_hmac
	$user_authenticated_balance = authenticate_user(array($snonce, $purchase_fridge, $user, $amount), $user, $user_hmac, $password_md5, $is_grad, $is_interfridge);
	if (!is_int($user_authenticated_balance)) {
		//Authentication failed.
		return error(6, "Error authenticating user: $user_authenticated_balance");
	}
	
	//Check whether local policy allows the remote topup
	$policy = policy_remote_topup($purchase_fridge, $user, $amount, $user_authenticated_balance, $is_interfridge, $is_grad);
	if ($policy !== null) {
		return error(7, $policy);
	}
	
	//All good, call the topup method on the remote fridge
	//Find the remote fridge
	$result = DBQuery("SELECT fridgeserver_endpoint, interfridge_password FROM users WHERE username='" . pg_escape_string($purchase_fridge) . "' AND enabled AND isinterfridge");
	if (pg_num_rows($result) == 0) {
		//No such fridge
		return error(10, "No such fridge $purchase_fridge.");
	}
	$row = pg_fetch_assoc($result);
	$purchase_fridge_endpoint = $row['fridgeserver_endpoint'];
	$purchase_fridge_password = $row['interfridge_password'];
	
	if (($remote_snonce = call_generate_nonce($purchase_fridge_endpoint, $LOCAL_FRIDGE_NAME, $purchase_fridge_password)) === null) {
		return error(11, "Error generating remote nonce ($purchase_fridge_endpoint, $LOCAL_FRIDGE_NAME, ***)."); //TODO: Make this less verbose
	}
	if (($remote_topup_result = call_topup($purchase_fridge_endpoint, $remote_snonce, $LOCAL_FRIDGE_NAME, $amount, $purchase_fridge_password)) === null) {
		return error(12, "Error making remote topup ($purchase_fridge_endpoint, $remote_snonce, $LOCAL_FRIDGE_NAME, $amount, ***)."); //TODO: Make this less verbose
	}
	
	//Transfer amount from fridge to local user
	if ($amount >= 0) {
		$transfer_errors = transferCredit($amount / 100.0, $purchase_fridge, $user);
	}
	else {
		$transfer_errors = transferCredit(-$amount / 100.0, $user, $purchase_fridge);
	}
	if (count($transfer_errors) != 0) {
		//Errors attempting to transfer
		$errors = implode(', ', $transfer_errors);
		return error(17, "Remote topup made, but errors transferring $amount from $purchase_fridge to $user: $errors");
	}
	
	//Get new balance
	$balance_row = pg_fetch_assoc(DBQuery("SELECT balance * 100 AS balance_i FROM users WHERE username='" . pg_escape_string($user) . "'"));
	$new_balance = (int) $balance_row['balance_i'];
	
	$fridge_response_hmac = sign($password_md5, array($snonce, $new_balance));
	return array('balance' => $new_balance, 'hmac' => $fridge_response_hmac);
}

//Proxy a fridgeserver method call to another peered fridge, to let web clients get around the same-origin policy
function proxy($method_name, $params, $app_data) {
	list($remote_fridge, $remote_method_name, $remote_args) = $params;
	
	//Find the remote fridge
	$result = DBQuery("SELECT fridgeserver_endpoint FROM users WHERE username='" . pg_escape_string($remote_fridge) . "' AND enabled AND isinterfridge");
	if (pg_num_rows($result) == 0) {
		//No such fridge
		return error(10, "No such fridge $remote_fridge.");
	}
	$row = pg_fetch_assoc($result);
	$remote_fridge_endpoint = $row['fridgeserver_endpoint'];
	
	//Make the call, returning the result
	$result = xmlrpc_call($remote_fridge_endpoint, $remote_method_name, $remote_args);
	if (is_null($result)) {
		return error(13, "Error proxying method call, remote fridge may not be responding.");
	}
	else if (xmlrpc_is_fault($result)) {
		//For some reason this is necessary to pass errors through correctly
		return error($result['faultCode'], $result['faultString']);
	}
	else {
		return $result;
	}
}

//Create XMLRPC server and register methods
$xmlrpc_server = xmlrpc_server_create();
xmlrpc_server_register_method($xmlrpc_server, 'generate_nonce', 'generate_nonce');
xmlrpc_server_register_method($xmlrpc_server, 'transfer', 'transfer');
xmlrpc_server_register_method($xmlrpc_server, 'get_stock', 'get_stock');
xmlrpc_server_register_method($xmlrpc_server, 'get_fridges', 'get_fridges');
xmlrpc_server_register_method($xmlrpc_server, 'get_fridge_name', 'get_fridge_name');
xmlrpc_server_register_method($xmlrpc_server, 'get_messages', 'get_messages');
xmlrpc_server_register_method($xmlrpc_server, 'get_user_info', 'get_user_info');
xmlrpc_server_register_method($xmlrpc_server, 'purchase', 'purchase');
xmlrpc_server_register_method($xmlrpc_server, 'topup', 'topup');
xmlrpc_server_register_method($xmlrpc_server, 'remote_purchase', 'remote_purchase');
xmlrpc_server_register_method($xmlrpc_server, 'remote_topup', 'remote_topup');
xmlrpc_server_register_method($xmlrpc_server, 'get_user_log', 'get_user_log');
xmlrpc_server_register_method($xmlrpc_server, 'proxy', 'proxy');

//Process the request and call the appropriate method, returning the response to the client
print xmlrpc_server_call_method($xmlrpc_server, $HTTP_RAW_POST_DATA, '');

//Clean up
xmlrpc_server_destroy($xmlrpc_server);
?>
