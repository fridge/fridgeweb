<?php
$cookie_check_override = true;
require_once("includes.php");

if ($_SESSION["fridge-admin-user"]) {
	// they're ok
}
elseif ($_SESSION["fridge-normal-user"] && ($_GET[username] == $_SESSION["fridge-normal-user"])) {
	// they're ok
}
else {
	die("You're not authorised to view the logs for that user.");
}

printstart("View log for $_GET[username]", "View log for $_GET[username]");
?>
<form action="userlog.php" method="get"><p>View last <input type="text" name="count" size="4" maxlength="5" /> transactions <input type="hidden" name="username" value="<?php print $_GET[username]; ?>" /><input type="submit" value="Show" /></p></form>
<form action="userlog.php" method="get"><p>View between <input type="text" name="datefrom" size="10" /> and <input type="text" name="dateto" size="10" /><input type="hidden" name="username" value="<?php print $_GET[username]; ?>" /> <input type="submit" value="Show" /></p></form>

<?php
// attempt to parse the stuff they gave us
if ($_GET[datefrom]) {
	$datefrom = strtotime($_GET[datefrom]);
	$dateto = strtotime($_GET[dateto]);
	
	if ($datefrom == -1 || $dateto == -1) {
		print "Could not parse one of the dates you specified. Try again!";
		printfinish("false");
		die();
	}
	else {
		// format them into SQL date strings
		$datefrom = date("Y-m-d H:m:s", $datefrom);
		$dateto = date("Y-m-d H:m:s", $dateto);
	}
}
elseif ($_GET[count]) {
	if (is_numeric($_GET[count]) && $_GET[count] >=0) {
		$count = $_GET[count];
	}
	else {
		print "Could not parse number of rows you wanted, was it numeric?";
		printfinish("false");
	}
}
?>

<table>
<tr>
	<td class="tableheader">Date/time</td>
	<td class="tableheader">Product Code</td>
	<td class="tableheader">Product Description</td>
	<td class="tableheader">Quantity</td>
	<td class="tableheader">Amount</td>
</tr>
<?php
// construct the sql
if ($count) {
	$sql = "select * from purchases where username='$_GET[username]' order by date_time desc limit $count";
}
elseif ($datefrom) {
	$sql = "select * from purchases where (date_time < '$dateto') and (date_time > '$datefrom') and username='$_GET[username]' order by date_time desc";
}
else {
	$onemonthago = date("Y-m-d H:m:s", strtotime("1 month ago"));
	$sql = "select * from purchases where username='$_GET[username]' and date_time > '$onemonthago' order by date_time desc";
}

$result = DBQuery($sql);
$colour = 0;
$lastdatetime = 0;

while ($row = pg_fetch_array($result)) {
	// when datetime changes, change the background colour (class) so order appears split up
	if ($lastdatetime != $row[date_time]) {
		$lastdatetime = $row[date_time];
		if ($colour == 0) {
			$class="list0";
			$colour++;
		}
		else {
			$class="list1";
			$colour = 0;
		}
	}

	print "<tr class='$class'>";
	print "<td>$row[date_time]</td>";
	print "<td>$row[product_code]</td>";
	print "<td>".DBQueryOnce("select description from product where product_code='$row[product_code]'", "description")."</td>";
	print "<td>$row[purchase_quantity]</td>";
	print "<td>\$$row[amount]</td></tr>";
	
	$total += $row[amount];
}
print "<tr><td></td><td></td><td></td><td>Total</td><td><b>\$$total</b></td></tr></table>";


// admins can return to menu, users can't
if ($_SESSION["fridge-admin-user"]) {
	printfinish(true);
}
else {
	printfinish(false);
}
?>
