<?php
header("Content-type: application/xml");
echo "<?xml version=\"1.0\"?>\n";
?>
<interfridge>
<?php
$cookie_check_override = true;
$no_session = true;
require_once("includes.php");
require_once("database_functions.php");

$NONCE_CHARS = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
$NONCE_LENGTH = 20;
$MAX_AGE = 600; //Maximum age of timestamp from client, in seconds. Note that server and client clocks may be a bit out.

//Attempt to authenticate the given (real or interfridge) user
//Return the user's balance if successfully authenticated, or else a string error message
function authenticate_user($message, $name, $hmac, $interfridge, &$password_md5) {
	$result = DBQuery("SELECT password_md5, balance * 100 AS balance_i FROM users WHERE username='" . pg_escape_string($name) . "' AND enabled AND isinterfridge = '" . ($interfridge ? 'true' : 'false') . "'");
	if (pg_num_rows($result) == 0) {
		//No such (enabled, interfridge if appropriate) user
		return "No such user $name.";
	}
	$row = pg_fetch_array($result);
	if ($hmac != hash_hmac('md5', $message, $row['password_md5'])) {
		return 'Invalid HMAC.';
	}
	
	//Successfully authenticated
	$password_md5 = $row['password_md5'];
	return (int) $row['balance_i'];
}

function send_error($error_message) {
?>
	<error>
		<?php echo $error_message; ?>
	</error>
<?php
}

function generate_nonce($cnonce, $timestamp, $fridge_name, $fridge_hmac) {
	global $MAX_AGE, $NONCE_CHARS, $NONCE_LENGTH;
	
	//Get rid of old nonces
	DBQuery("DELETE FROM nonces WHERE created_at < NOW() - INTERVAL '10 minutes'");
	
	//Check the timestamp
	if (time() - $timestamp > $MAX_AGE) {
		send_error('Timestamp too old.');
		return;
	}
	//Check client nonce (namely that it is not already associated with a server nonce)
	$result = pg_fetch_array(DBQuery("SELECT COUNT(*) FROM nonces WHERE cnonce='" . pg_escape_string($cnonce) . "'", 0));
	if ($result[0] != 0) {
		send_error('Invalid client nonce.');
		return;
	}
	//Check the HMAC from the client
	$fridge_authenticated_balance = authenticate_user("$cnonce,$timestamp,$fridge_name", $fridge_name, $fridge_hmac, true, $fridge_password_md5);
	if (!is_int($fridge_authenticated_balance)) {
		//Authentication failed.
		send_error("Error authenticating fridge for nonce: $fridge_authenticated_balance");
		return;
	}
	
	//Generate a nonce
	$snonce = '';
	for ($i = 0; $i < $NONCE_LENGTH; ++$i) {
		$snonce .= substr($NONCE_CHARS, rand(0, strlen($NONCE_CHARS) - 1), 1);
	}
	DBQuery("INSERT INTO nonces (snonce, cnonce) VALUES('" . pg_escape_string($snonce) . "', '" . pg_escape_string($cnonce) . "')");
	
	//Calculate the HMAC
	$fridge_response_hmac = hash_hmac('md5', "$snonce,$cnonce", $fridge_password_md5);
?>
	<nonce>
		<?php echo $snonce; ?>
	</nonce>
	<hmac>
		<?php echo $fridge_response_hmac; ?>
	</hmac>
<?php
}

function purchase($snonce, $fridge_name, $user_name, $amount, $fridge_hmac, $user_hmac) {
	//Check that nonce is valid
	$result = pg_fetch_array(DBQuery("SELECT COUNT(*) FROM nonces WHERE snonce='" . pg_escape_string($snonce) . "'", 0));
	if ($result[0] != 1) {
		send_error('Invalid nonce.');
		return;
	}
	//Nonce is valid, delete it to prevent replays
	DBQuery("DELETE FROM nonces WHERE snonce='" . pg_escape_string($nonce) . "'");
	
	//Authenticate the fridge by the fridge_hmac
	$fridge_authenticated_balance = authenticate_user("$snonce,$fridge_name,$user_name,$amount", $fridge_name, $fridge_hmac, true, $fridge_password_md5);
	if (!is_int($fridge_authenticated_balance)) {
		//Authentication failed.
		send_error("Error authenticating fridge for purchase: $fridge_authenticated_balance");
		return;
	}
	
	//Authenticate the user by the user_hmac
	$user_authenticated_balance = authenticate_user("$snonce,$fridge_name,$user_name,$amount", $user_name, $user_hmac, false);
	if (!is_int($user_authenticated_balance)) {
		//Authentication failed.
		send_error("Error authenticating user: $user_authenticated_balance");
		return;
	}
	
	//Everything is authenticated fine, now check whether the user has enough balance to make the purchase
	if ($user_authenticated_balance - $amount < 0 && $amount > 0) {
		send_error("Insufficient credit: user's balance is $user_authenticated_balance.");
		return;
	}
	//Make the purchase by transferring from the user's account to the remote fridge's interfridge account
	if ($amount > 0) {
		$transfer_errors = transferCredit($amount / 100.0, $user_name, $fridge_name);
	}
	else if ($amount < 0) {
		//Amount is negative so transfer from fridge to user instead
		$transfer_errors = transferCredit(-$amount / 100.0, $fridge_name, $user_name);
	}
	else {
		//Amount is 0, which is used for authenticating a user without making a purchase
		$transfer_errors = array();
	}
	if (count($transfer_errors) != 0) {
		//Errors attempting to transfer
?>
	<error>
		Error transferring from user to interfridge account:
<?php
		foreach ($transfer_errors as $error) {
			echo $error;
		}
?>
	</error>
<?php
		return;
	}
	//Success! Return the user's new balance after making the purchase. We also need to calculate the HMAC for this.
	$new_balance = $user_authenticated_balance - $amount;
	$fridge_response_hmac = hash_hmac('md5', "$snonce,$fridge_name,$user_name,$new_balance", $fridge_password_md5);
?>
	<balance>
		<?php echo $new_balance; ?>
	</balance>
	<hmac>
		<?php echo $fridge_response_hmac; ?>
	</hmac>
<?php
}

$method = $_GET['method'];

if ($method == 'generate_nonce') {
	$cnonce = $_GET['cnonce'];
	$timestamp = (int) $_GET['timestamp'];
	$fridge_name = $_GET['fridge_name'];
	$fridge_hmac = $_GET['fridge_hmac'];
	generate_nonce($cnonce, $timestamp, $fridge_name, $fridge_hmac);
}
else if ($method == 'purchase') { //TODO: This should be a POST rather than a GET, as it changes state
	$snonce = $_GET['nonce'];
	$fridge_name = $_GET['fridge_name'];
	$user_name = $_GET['user_name'];
	$amount = (int) $_GET['amount'];
	$fridge_hmac = $_GET['fridge_hmac'];
	$user_hmac = $_GET['user_hmac'];
	purchase($snonce, $fridge_name, $user_name, $amount, $fridge_hmac, $user_hmac);
}
else {
	send_error('Unrecognised method.');
}
?>
</interfridge>
