--
-- PostgreSQL database dump
--

SET client_encoding = 'LATIN1';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- Name: reports; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA reports;


--
-- Name: plpgsql; Type: PROCEDURAL LANGUAGE; Schema: -; Owner: -
--

CREATE PROCEDURAL LANGUAGE plpgsql;


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = true;

--
-- Name: adjustments; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE adjustments (
    date_time timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL,
    product_code character varying(4) NOT NULL,
    est_cost numeric(9,2) NOT NULL,
    quantity integer DEFAULT (-1) NOT NULL
);


SET default_with_oids = false;

--
-- Name: balance_log; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE balance_log (
    date_time timestamp without time zone,
    drawer numeric(9,2),
    products numeric(9,2),
    users_positive numeric(9,2),
    users_negative numeric(9,2),
    "position" numeric(9,2)
);


SET default_with_oids = true;

--
-- Name: barcode_product; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE barcode_product (
    barcode character varying(20) NOT NULL,
    product_code character varying(4)
);


--
-- Name: barcode_user; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE barcode_user (
    barcode character varying(30) NOT NULL,
    username character varying(20)
);


--
-- Name: category; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE category (
    category_id integer DEFAULT nextval(('public.category_category_id_seq'::text)::regclass) NOT NULL,
    display_sequence integer NOT NULL,
    title character varying(25) NOT NULL
);


--
-- Name: internal_variables; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE internal_variables (
    variable character varying(30) NOT NULL,
    value character varying(30)
);


SET default_with_oids = false;

--
-- Name: nonces; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE nonces (
    snonce text NOT NULL,
    cnonce text NOT NULL,
    created_at timestamp with time zone DEFAULT now()
);


SET default_with_oids = true;

--
-- Name: numerical_variables; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE numerical_variables (
    variable character varying(30) NOT NULL,
    value numeric(10,2) NOT NULL
);


--
-- Name: product; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE product (
    product_code character varying(4) NOT NULL,
    description character varying(60) NOT NULL,
    cost numeric(9,2) NOT NULL,
    markup numeric(4,1) DEFAULT 0 NOT NULL,
    enabled boolean DEFAULT false NOT NULL,
    category_id integer NOT NULL,
    in_stock integer DEFAULT 0 NOT NULL,
    stock_low_mark integer DEFAULT 10 NOT NULL
);


--
-- Name: purchase_units; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE purchase_units (
    product_code character varying(4) NOT NULL,
    unit_description character varying(16),
    quantity integer NOT NULL,
    CONSTRAINT purchase_units_quantity CHECK ((quantity > 0))
);


--
-- Name: purchase_units_default; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE purchase_units_default (
    product_code character varying(4) NOT NULL,
    quantity integer NOT NULL
);


--
-- Name: purchases; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE purchases (
    username character varying(20) NOT NULL,
    product_code character varying(4) NOT NULL,
    date_time timestamp without time zone NOT NULL,
    purchase_quantity integer NOT NULL,
    amount numeric(9,2) NOT NULL,
    surplus numeric(9,2) NOT NULL
);


SET search_path = reports, pg_catalog;

--
-- Name: item_purchase_units; Type: VIEW; Schema: reports; Owner: -
--

CREATE VIEW item_purchase_units AS
    SELECT a.p1pc AS product_code, a.p1q AS quantity, a.unit_description FROM (SELECT p1.product_code AS p1pc, p1.quantity AS p1q, p2.product_code AS p2pc, p2.quantity AS p2q, p1.unit_description FROM (public.purchase_units p1 LEFT JOIN public.purchase_units_default p2 ON (((p1.product_code)::text = (p2.product_code)::text)))) a WHERE ((((a.p1pc)::text = (a.p2pc)::text) AND (a.p1q = a.p2q)) OR (a.p2q IS NULL));


SET search_path = public, pg_catalog;

--
-- Name: purchase_statistics; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW purchase_statistics AS
    SELECT a.product_code, product.description, ((a.sum)::double precision / b.numdays) AS averageperday, ceil(((item_purchase_units.quantity)::double precision / ((a.sum)::double precision / b.numdays))) AS daysperunit, floor((((a.sum)::double precision / b.numdays) / (item_purchase_units.quantity)::double precision)) AS unitsperday, floor(((((a.sum)::double precision / b.numdays) / (item_purchase_units.quantity)::double precision) * (7)::double precision)) AS unitsperweek, floor(((((a.sum)::double precision / b.numdays) / (item_purchase_units.quantity)::double precision) * (29.5)::double precision)) AS unitspermonth, item_purchase_units.quantity AS itemsperunit, item_purchase_units.unit_description FROM (((SELECT purchases.product_code, sum(purchases.purchase_quantity) AS sum FROM purchases GROUP BY purchases.product_code HAVING (sum(purchases.purchase_quantity) > 0)) a NATURAL JOIN product) NATURAL LEFT JOIN reports.item_purchase_units), (SELECT date_part('day'::text, (now() - (min(purchases.date_time))::timestamp with time zone)) AS numdays FROM purchases) b;


--
-- Name: requests; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE requests (
    id integer DEFAULT nextval(('public.requests_id_seq'::text)::regclass) NOT NULL,
    descr character varying(100)
);


--
-- Name: stock_alters; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE stock_alters (
    date_time timestamp without time zone NOT NULL,
    product_code character varying(4) NOT NULL,
    stock_difference integer NOT NULL
);


--
-- Name: stored_queries; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE stored_queries (
    id integer DEFAULT nextval(('public.stored_queries_id_seq'::text)::regclass) NOT NULL,
    name character varying(100) NOT NULL,
    query_base64 text NOT NULL
);


--
-- Name: user_credit_log; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE user_credit_log (
    username character varying(20) NOT NULL,
    date_time timestamp without time zone NOT NULL,
    amount numeric(9,2) NOT NULL,
    transaction_type character varying(10) NOT NULL,
    transfer_received_from character varying(20),
    transfer_sent_to character varying(20),
    CONSTRAINT valid_user_transaction_type CHECK (((((((transaction_type)::text = ('CREDIT'::character varying)::text) OR ((transaction_type)::text = ('PURCHASE'::character varying)::text)) OR (((transaction_type)::text = ('XFER-OUT'::character varying)::text) AND NULL::boolean)) OR (((transaction_type)::text = ('XFER-IN'::character varying)::text) AND NULL::boolean)) OR ((transaction_type)::text = ('ADMIN'::character varying)::text)))
);


--
-- Name: user_requests; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE user_requests (
    reqid integer NOT NULL,
    username character varying(50) NOT NULL
);


--
-- Name: users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE users (
    username character varying(20) NOT NULL,
    password_md5 character varying(35) NOT NULL,
    real_name character varying(60),
    email_address character varying(60),
    balance numeric(9,2) NOT NULL,
    isadmin boolean DEFAULT false NOT NULL,
    isgrad boolean DEFAULT false NOT NULL,
    sponsor character varying(20),
    enabled boolean DEFAULT true,
    isinterfridge boolean DEFAULT false NOT NULL,
    interfridge_password character varying(20) DEFAULT NULL::character varying,
    interfridge_endpoint text,
    fridgeserver_endpoint text
);


SET search_path = reports, pg_catalog;

--
-- Name: average_daily_purchases; Type: VIEW; Schema: reports; Owner: -
--

CREATE VIEW average_daily_purchases AS
    SELECT a.product_code, product.description, ((a.sum)::double precision / b.numdays) AS average FROM ((SELECT purchases.product_code, sum(purchases.purchase_quantity) AS sum FROM public.purchases GROUP BY purchases.product_code HAVING (sum(purchases.purchase_quantity) > 0)) a NATURAL JOIN public.product), (SELECT date_part('day'::text, (now() - (min(purchases.date_time))::timestamp with time zone)) AS numdays FROM public.purchases) b;


--
-- Name: bollocksfuck; Type: VIEW; Schema: reports; Owner: -
--

CREATE VIEW bollocksfuck AS
    SELECT a.product_code, a.description, a.cost, a.markup, a.sellprice, a.surplus, a.in_stock, a.stock_value, ((a.in_stock)::numeric * a.sellprice) AS sale_value, ((a.in_stock)::numeric * a.surplus) AS surplus_value FROM (SELECT product.product_code, product.description, product.cost, product.markup, ((round((product.cost * (100.0 + product.markup))) / 100.0))::numeric(9,2) AS sellprice, ((round((product.cost * product.markup)) / 100.0))::numeric(9,2) AS surplus, product.in_stock, ((product.in_stock)::numeric * product.cost) AS stock_value FROM public.product WHERE (product.enabled = true)) a;


--
-- Name: dates_purchase_stats; Type: VIEW; Schema: reports; Owner: -
--

CREATE VIEW dates_purchase_stats AS
    SELECT date_part('year'::text, purchases.date_time) AS year, date_part('month'::text, purchases.date_time) AS month, date_part('day'::text, purchases.date_time) AS day, date_part('week'::text, purchases.date_time) AS week, purchases.product_code, purchases.purchase_quantity, purchases.username, purchases.amount FROM public.purchases;


--
-- Name: monthly_purchase_sums; Type: VIEW; Schema: reports; Owner: -
--

CREATE VIEW monthly_purchase_sums AS
    SELECT dates_purchase_stats.year, dates_purchase_stats.month, dates_purchase_stats.product_code, sum(dates_purchase_stats.purchase_quantity) AS count FROM dates_purchase_stats GROUP BY dates_purchase_stats.year, dates_purchase_stats.month, dates_purchase_stats.product_code ORDER BY dates_purchase_stats.year DESC, dates_purchase_stats.month DESC, sum(dates_purchase_stats.purchase_quantity) DESC;


--
-- Name: product_info; Type: VIEW; Schema: reports; Owner: -
--

CREATE VIEW product_info AS
    SELECT a.product_code, a.description, a.cost, a.markup, a.sellprice, a.surplus AS item_surplus, a.in_stock, a.stock_value, ((a.in_stock)::numeric * a.sellprice) AS sale_value, ((a.in_stock)::numeric * a.surplus) AS surplus_value FROM (SELECT product.product_code, product.description, product.cost, product.markup, ((round((product.cost * (100.0 + product.markup))) / 100.0))::numeric(9,2) AS sellprice, ((round((product.cost * product.markup)) / 100.0))::numeric(9,2) AS surplus, product.in_stock, ((product.in_stock)::numeric * product.cost) AS stock_value FROM public.product WHERE (product.enabled = true)) a;


--
-- Name: predictions; Type: VIEW; Schema: reports; Owner: -
--

CREATE VIEW predictions AS
    SELECT a.product_code, product.description, (((a.totalsold)::double precision / b.numdays))::numeric(4,2) AS averageperday, product.in_stock, (((product.in_stock)::double precision / (((a.totalsold)::double precision / b.numdays) + (0.0000001)::double precision)))::numeric(4,0) AS numdaysremain, ceil(((item_purchase_units.quantity)::double precision / ((a.totalsold)::double precision / b.numdays))) AS daysperunit, floor((((a.totalsold)::double precision / b.numdays) / (item_purchase_units.quantity)::double precision)) AS unitsperday, floor(((((a.totalsold)::double precision / b.numdays) / (item_purchase_units.quantity)::double precision) * (7)::double precision)) AS unitsperweek, floor(((((a.totalsold)::double precision / b.numdays) / (item_purchase_units.quantity)::double precision) * (29.5)::double precision)) AS unitspermonth, item_purchase_units.quantity AS itemsperunit, item_purchase_units.unit_description, floor((((((a.totalsold)::double precision / b.numdays) / (item_purchase_units.quantity)::double precision) * (29.5)::double precision) * (item_purchase_units.quantity)::double precision)) AS itemspermonth, (floor((((((a.totalsold)::double precision / b.numdays) / (item_purchase_units.quantity)::double precision) * (29.5)::double precision) * (item_purchase_units.quantity)::double precision)) * (a.item_surplus)::double precision) AS surpluspermonth FROM (((SELECT purchases.product_code, sum(purchases.purchase_quantity) AS totalsold, product_info.item_surplus FROM (public.purchases NATURAL JOIN product_info) GROUP BY purchases.product_code, product_info.item_surplus HAVING (sum(purchases.purchase_quantity) > 0)) a NATURAL JOIN public.product) NATURAL LEFT JOIN item_purchase_units), (SELECT date_part('day'::text, (now() - (min(purchases.date_time))::timestamp with time zone)) AS numdays FROM public.purchases) b;


--
-- Name: weekly_purchase_sums; Type: VIEW; Schema: reports; Owner: -
--

CREATE VIEW weekly_purchase_sums AS
    SELECT dates_purchase_stats.year, dates_purchase_stats.week, dates_purchase_stats.product_code, sum(dates_purchase_stats.purchase_quantity) AS count FROM dates_purchase_stats GROUP BY dates_purchase_stats.year, dates_purchase_stats.week, dates_purchase_stats.product_code ORDER BY dates_purchase_stats.year DESC, dates_purchase_stats.week DESC, sum(dates_purchase_stats.purchase_quantity) DESC;


SET search_path = public, pg_catalog;

--
-- Name: surplus_for_product(text, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION surplus_for_product(text, text) RETURNS numeric
    AS $_$ SELECT (round(product.cost * (product.markup + (CASE WHEN (SELECT isgrad FROM users WHERE username = $2) THEN 0 ELSE (SELECT value FROM numerical_variables WHERE variable = 'graduate_discount') END) )) / 100.0)::numeric(9,2) AS surplus from product where product_code = $1 $_$
    LANGUAGE sql;


SET search_path = reports, pg_catalog;

--
-- Name: days_on_fridge(text); Type: FUNCTION; Schema: reports; Owner: -
--

CREATE FUNCTION days_on_fridge(username text) RETURNS integer
    AS $_$ select extract(days from current_timestamp - reports.first_purchase_date($1))::int; $_$
    LANGUAGE sql;


--
-- Name: first_purchase_date(text); Type: FUNCTION; Schema: reports; Owner: -
--

CREATE FUNCTION first_purchase_date(username text) RETURNS timestamp without time zone
    AS $_$select date_time from purchases where username = $1 order by date_time asc limit 1;$_$
    LANGUAGE sql;


SET search_path = public, pg_catalog;

--
-- Name: array_accum(anyelement); Type: AGGREGATE; Schema: public; Owner: -
--

CREATE AGGREGATE array_accum(anyelement) (
    SFUNC = array_append,
    STYPE = anyarray,
    INITCOND = '{}'
);


--
-- Name: arrays_accum(anyarray); Type: AGGREGATE; Schema: public; Owner: -
--

CREATE AGGREGATE arrays_accum(anyarray) (
    SFUNC = array_cat,
    STYPE = anyarray,
    INITCOND = '{}'
);


--
-- Name: text_arrays_accum(text[]); Type: AGGREGATE; Schema: public; Owner: -
--

CREATE AGGREGATE text_arrays_accum(text[]) (
    SFUNC = array_cat,
    STYPE = text[],
    INITCOND = '{}'
);


--
-- Name: category_category_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE category_category_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- Name: category_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE category_category_id_seq OWNED BY category.category_id;


--
-- Name: requests_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE requests_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- Name: requests_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE requests_id_seq OWNED BY requests.id;


--
-- Name: stored_queries_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE stored_queries_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- Name: stored_queries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE stored_queries_id_seq OWNED BY stored_queries.id;


--
-- Name: adjustments_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY adjustments
    ADD CONSTRAINT adjustments_pkey PRIMARY KEY (date_time, product_code);


--
-- Name: barcode_product_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY barcode_product
    ADD CONSTRAINT barcode_product_pkey PRIMARY KEY (barcode);


--
-- Name: barcode_user_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY barcode_user
    ADD CONSTRAINT barcode_user_pkey PRIMARY KEY (barcode);


--
-- Name: category_display_sequence_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY category
    ADD CONSTRAINT category_display_sequence_key UNIQUE (display_sequence);


--
-- Name: category_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY category
    ADD CONSTRAINT category_pkey PRIMARY KEY (category_id);


--
-- Name: internal_variables_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY internal_variables
    ADD CONSTRAINT internal_variables_pkey PRIMARY KEY (variable);


--
-- Name: nonces_cnonce_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY nonces
    ADD CONSTRAINT nonces_cnonce_key UNIQUE (cnonce);


--
-- Name: nonces_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY nonces
    ADD CONSTRAINT nonces_pkey PRIMARY KEY (snonce);


--
-- Name: numerical_variables_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY numerical_variables
    ADD CONSTRAINT numerical_variables_pkey PRIMARY KEY (variable);


--
-- Name: product_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_pkey PRIMARY KEY (product_code);


--
-- Name: purchase_units_default_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY purchase_units_default
    ADD CONSTRAINT purchase_units_default_pkey PRIMARY KEY (product_code, quantity);


--
-- Name: purchase_units_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY purchase_units
    ADD CONSTRAINT purchase_units_pkey PRIMARY KEY (product_code, quantity);


--
-- Name: purchases_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY purchases
    ADD CONSTRAINT purchases_pkey PRIMARY KEY (username, product_code, date_time);


--
-- Name: requests_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY requests
    ADD CONSTRAINT requests_pkey PRIMARY KEY (id);


--
-- Name: stock_alters_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY stock_alters
    ADD CONSTRAINT stock_alters_pkey PRIMARY KEY (date_time, product_code);


--
-- Name: stored_queries_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY stored_queries
    ADD CONSTRAINT stored_queries_pkey PRIMARY KEY (id);


--
-- Name: user_credit_log_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY user_credit_log
    ADD CONSTRAINT user_credit_log_pkey PRIMARY KEY (username, date_time);


--
-- Name: user_requests_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY user_requests
    ADD CONSTRAINT user_requests_pkey PRIMARY KEY (reqid, username);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (username);


--
-- Name: purchase_month_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX purchase_month_idx ON purchases USING btree (date_part('year'::text, date_time), date_part('month'::text, date_time));


--
-- Name: purchase_week_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX purchase_week_idx ON purchases USING btree (date_part('year'::text, date_time), date_part('week'::text, date_time));


--
-- Name: $1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY product
    ADD CONSTRAINT "$1" FOREIGN KEY (category_id) REFERENCES category(category_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: $1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY barcode_product
    ADD CONSTRAINT "$1" FOREIGN KEY (product_code) REFERENCES product(product_code) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: $1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY purchases
    ADD CONSTRAINT "$1" FOREIGN KEY (username) REFERENCES users(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: $1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY stock_alters
    ADD CONSTRAINT "$1" FOREIGN KEY (product_code) REFERENCES product(product_code) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: $1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_credit_log
    ADD CONSTRAINT "$1" FOREIGN KEY (username) REFERENCES users(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: $1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY barcode_user
    ADD CONSTRAINT "$1" FOREIGN KEY (username) REFERENCES users(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: $1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY purchase_units
    ADD CONSTRAINT "$1" FOREIGN KEY (product_code) REFERENCES product(product_code) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: $1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY purchase_units_default
    ADD CONSTRAINT "$1" FOREIGN KEY (product_code, quantity) REFERENCES purchase_units(product_code, quantity) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: $1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_requests
    ADD CONSTRAINT "$1" FOREIGN KEY (reqid) REFERENCES requests(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: $1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY adjustments
    ADD CONSTRAINT "$1" FOREIGN KEY (product_code) REFERENCES product(product_code) ON UPDATE CASCADE;


--
-- Name: $2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY purchases
    ADD CONSTRAINT "$2" FOREIGN KEY (product_code) REFERENCES product(product_code) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: $2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_credit_log
    ADD CONSTRAINT "$2" FOREIGN KEY (transfer_received_from) REFERENCES users(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: $2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_requests
    ADD CONSTRAINT "$2" FOREIGN KEY (username) REFERENCES users(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: $3; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_credit_log
    ADD CONSTRAINT "$3" FOREIGN KEY (transfer_sent_to) REFERENCES users(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: sponsor_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT sponsor_fk FOREIGN KEY (sponsor) REFERENCES users(username);


--
-- Name: public; Type: ACL; Schema: -; Owner: -
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

