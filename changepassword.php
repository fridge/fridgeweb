<?php
require_once("includes.php");
require_once("database_functions.php");

printstart("Change password", "Change password for " . $_REQUEST[username]);

if ($_POST) {
	// check if the entered passwords match
	if ($_POST[pass1] == $_POST[pass2]) {
		// make the change
		changePassword($_REQUEST[username], $_REQUEST[pass1]);
		
		//close javascript window
		print "<html><head><script type='text/javascript'>window.close();</script></head><body></body></html>";
		die();
	}
	else {
		print "<h2 class='loginerror'>Passwords did not match! Please try again</h2>";
	}
}
?>

<p style="text-align: center;">Enter a new password for the user.</p>

<form action="changepassword.php" method="post">
<input type="hidden" name="username" value="<?php print $_REQUEST[username]; ?>" />
<table>
<tr>
	<td class="tableheader">Password:</td>
	<td><input type="password" name="pass1" size="10" maxlength="20" /></td>
</tr>
<tr>
	<td class="tableheader">Verify:</td>
	<td><input type="password" name="pass2" size="10" maxlength="20" /></td>
</tr>
<tr>
	<td colspan="2"><input type="submit" value="Change" /></td>
</tr>
</table>
</form>

<?php printfinish(false); ?>
