<?php
$cookie_check_override = true;
require_once("includes.php");

if ($_SESSION["fridge-admin-user"]) {
        $username = $_SESSION["fridge-admin-user"];
}
elseif ($_SESSION["fridge-normal-user"]) {
        $username = $_SESSION["fridge-normal-user"];
}
else {
        die("You have no authenticated yourself. Please <a href='index.php'>login</a> and make sure cookies are on!");
}

// Process any input that may have arrived
if ($_POST[newwish]) {
	// this is a new request
	DBQuery("insert into requests (descr) values ('$_POST[newwish]')");
	$newid = DBQueryOnce("select max(id) as thisid from requests", "thisid"); // anyone think of a better way to read in the serial?
	DBQuery("insert into user_requests(reqid, username) values ('$newid', '$username')");
}
else if ($_GET) {
	if ($_GET[action] == "vote") {
		DBQuery("insert into user_requests (reqid, username) values ('$_GET[request]', '$username')");
	}
	elseif ($_GET[action] == "remove" && $_SESSION["fridge-admin-user"]) {
		DBQuery("delete from requests where id='$_GET[request]'"); // will cascade to user_requests table
	}
}

printstart("Wishlist", "Wishlist");

// show current wishlist with option to agree
?>
<h2>Make a wish</h2>
<p>If there's something the fridge should sell or do, make sure someone else hasn't already thought of it (see below). If it's new, then enter it below. If lots of people vote for it, or it's easy, it'll probably happen!</p>
<form style="text-align: center;" action="wishlist.php" method="post">
Request <input type="text" name="newwish" size="30" maxlength="100" />&nbsp;<input type="submit" value="Place request" />
</form>

<h2>Current Wishlist</h2>

<table>
<tr>
	<td class="tableheader">Request</td><td class="tableheader">Supporters</td><td class="tableheader">Options</td></tr>
<?php
$result = DBQuery("select * from requests");

$outarray = array();

while ($row = pg_fetch_array($result)) {
	// find out a list of people who support this request
	$requestors = array();
	$requestors_result = DBQuery("select * from user_requests where reqid='$row[id]' order by username");
	while ($reqrow = pg_fetch_array($requestors_result)) {
		$requestors[] = $reqrow[username];
	}
	
	// output array is in format reqID, description, requestor username array, number of requestors
	$outarray[] = array($row[id], $row[descr], $requestors, count($requestors));
}

// So we now have an array with all the data we need, display it
function cmp($a, $b) {
	// compare
	if ($a[3] > $b[3]) return -1;
	else if ($a[3] == $b[3]) return 0;
	else return 1;
}
usort($outarray, "cmp"); // sort by the number of requestors

$line = 0;
foreach ($outarray as $arrayrow) {
	print "<tr class='list$line'><td>$arrayrow[1]</td><td>";
	$line == 0 ? $line++ : $line = 0;

	foreach ($arrayrow[2] as $user) {
		print DBQueryOnce("select real_name from users where username='$user'", "real_name")."<br />";
	}
	print "</td><td>";
	if (!DBQueryOnce("select * from user_requests where username='$username' and reqid='$arrayrow[0]'", "username")) {
		// The current user has not already voted for this product. display voting link.
		print "<a href='wishlist.php?action=vote&amp;request=$arrayrow[0]'>Vote for this</a><br />";
	}
	if ($_SESSION["fridge-admin-user"]) {
		// Admin users can delete this entry (once it's been added to the fridge! :D)
		print "<a href='wishlist.php?action=remove&amp;request=$arrayrow[0]'>Remove from list (admin option)</a>";
	}
	print "</td></tr>";
}

print "</table>";

printfinish(true);
?>
