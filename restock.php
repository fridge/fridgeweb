<?php
require_once("includes.php");
require_once("database_functions.php");

// config
$number_of_rows_to_allow = 100; // maximum number of rows that it will expand to. these are all written into the html, so don't make it too big!

// handle submission if we have one 
if ($_POST) {
	// don't bother checking, so long as javascript has stamped it ok
	if ($_POST["javascript-checked"] != "1") {
		die("The restock you submitted could not be processed because javascript did not mark it as pre-checked. Make sure javascript is enabled!");
	}
	
	for ($i = 0; $i < $number_of_rows_to_allow; $i++) {
		if ($_POST["code-$i"] != "") {
			// this is a valid row, update it
			
			// alter the price by a weight based on the old cost and stock level (see database_functions.php)
			alterProductCost($_POST["code-$i"], $_POST["postcost-$i"], $_POST["qty-$i"]);
			
			// update the quantity
			alterProductQuantity($_POST["code-$i"], $_POST["qty-$i"]);
		}
	}
	
	// and finally, debit the drawer balance by whatever
	debitDrawerBalance($_POST["debit-drawer"]);

	// return the user to the menu
	redirect("menu.php?message=Fridge restock applied");
}
			

printstart("Restock Fridge", "Restock Fridge");


?>
<p style="text-align: center;"><b>Note: This requires Javascript 1.5 to round prices. Browsers such as IE and Safari won't do this.<br />This page won't work at all without Javascript!!</b></p>

<form action="restock.php" method="post" id="form" onsubmit="return checkBeforeSubmit();">
<table>
<tr>
	<td colspan="3">Keyed costs will be GST <select id="gst_select" onchange='recalculateTable();'><option selected="selected" value="inc">Inclusive</option><option value="ex">Exclusive</option></select></td>
</tr>
<tr>
	<td class="tableheader">Description</td><td class="tableheader">Code</td><td class="tableheader">Qty</td><td class="tableheader">Cost ea.</td><td class="tableheader">Cost ea. inc. gst</td><td class="tableheader">Total cost</td>
</tr>

<?php
	// print all entry rows (of which 1 will be initially be shown)
	for ($i = 0; $i < $number_of_rows_to_allow; $i++) {
		if (($i % 2) == 0) {
			$rowclass = "list0";
		}
		else {
			$rowclass = "list1";
		}
	
		print "<tr id='entryrow-$i' style='display: none;' class='$rowclass'>
			<td><input type='text' disabled='disabled' size='25' id='descr-$i' class='phantom' /></td>
			<td><input type='text' size='5' name='code-$i' id='code-$i' onchange='processCode($i);recalculateTable();' onfocus='enteredRow($i)' /></td>
			<td><input type='text' size='4' name='qty-$i' id='qty-$i' onchange='processQty($i);recalculateTable();' /></td>
			<td><input type='text' size='6' id='precost-$i' onchange='processCost($i);recalculateTable();' /></td>
			<td><input type='text' size='6' disabled='disabled' name='cost-$i' class='phantom' id='cost-$i' /><input type='hidden' name='postcost-$i' id='postcost-$i' /></td>
			<td><input type='text' size='8' id='extended-cost-$i' onchange='calculateBackwardsFromTotalRowCost($i);' />
				<input type='hidden' size='1' id='codeerror-$i' value='0' />
				<input type='hidden' size='1' id='qtyerror-$i' value='0' />
				<input type='hidden' size='1' id='precosterror-$i' value='0' />
				<input type='hidden' size='1' id='extendedcosterror-$i' value='0' />
			</td>
			<td><a onclick='javascript: clearRow($i); recalculateTable()'>Clear row</a></td>
		       </tr>";
	}
	
?>

<tr>
	<td>&nbsp;</td>
</tr>
<tr>
	<td>Order total (inc GST)</td><td><input id="totalcost" type="text" size="6" name="totalcost" disabled="disabled" class="phantom" value="0" /></td>
</tr>
<tr>
	<td>Debit drawer balance by</td><td><input type="text" id="debit-drawer" name="debit-drawer" size="6" value="0" />
	<input type="hidden" name="javascript-checked" id="javascript-checked" value="0" />
	</td><td colspan="3">
	<input type="submit" value="Commit Restock" />
	</td>
</tr>

</table>
</form>


<script type="text/javascript">
<!--
// isNumeric function modified from code at http://www.codetoad.com/javascript/isnumeric.asp
function isNumeric(sText, decimalallowed)
{
   if (decimalallowed) {
	var ValidChars = "0123456789.";
   }
   else {
	var ValidChars = "0123456789";
   }
   var IsNumber=true;
   var Char;

   for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {
         IsNumber = false;
         }
      }
      return IsNumber;
   
}

// Set there to be one row visible initially
var rowsvisible = 2;
var numrows = <?php print $number_of_rows_to_allow; ?>;
var ordertotal = 0;

// Unhide the first two rows
document.getElementById("entryrow-0").style.display = "";
document.getElementById("entryrow-1").style.display = "";

// Populate the products arrays
<?php
$products = array();
$result = DBQuery("select product_code, description from product where enabled='t' order by product_code");
while ($row = filterArray(pg_fetch_array($result))) {
	$products[] = array($row[product_code], $row[description]);
}
?>

// note we enter a dummy -1 at the beginning of each array that isn't scanned in order to insert commas between
// elements easily (I'm lazy)
var product_codes = new Array(-1<?php 
			foreach ($products as $product) {
				print ",\"$product[0]\"";
			} ?>);
var product_descriptions = new Array(-1<?php 
			foreach ($products as $product) {
				print ",\"".htmlentities($product[1])."\"";
			} ?>);

function processCode(rownum) {
	// Convert box to uppercase 
	var box = document.getElementById("code-" + rownum).value.toUpperCase();
	document.getElementById("code-" + rownum).value = box; // save back to box
	
	// When we lose focus on the code box, check it and fill in the description box
	for (i = 1; i < product_codes.length; i++) {
		if (product_codes[i] == box) {
			//we've found it
			document.getElementById("descr-" + rownum).value = product_descriptions[i];
			// reset the colour code error if it was previosly invalid
			clearCodeError(rownum);
			
			return true;
		}
	}
	// doesn't appear to be a valid code, have they made it blank (that's ok!)
	if (box == "") {
		// clear all other fields
		document.getElementById("code-" + rownum).value = "";
		document.getElementById("descr-" + rownum).value = "";
		document.getElementById("qty-" + rownum).value = "";
		document.getElementById("precost-" + rownum).value = "";
		document.getElementById("cost-" + rownum).value = "";
		document.getElementById("extended-cost-" + rownum).value = "";
		
		clearCodeError(rownum);
		clearQtyError(rownum);
		clearPrecostError(rownum);
		clearExtendedcostError(rownum);
			
		return true;
	}
	
	// otherwise it wasn't valid, make it red and set error flag
	setCodeError(rownum);
	return false;
}

function processQty(rownum) {
	// make sure it's a positive number (this function implicitly disallows negative numbers by disallowing the '-' character
	if (isNumeric(document.getElementById("qty-" + rownum).value, false)) {
		// this is a valid non-decimal number, reset any previous error
		clearQtyError(rownum);
		return true;
	}
	else {
		setQtyError(rownum);
		return false;
	}
}

function processCost(rownum) {
	// make sure it's a positive number (this function implicitly disallows negative numbers by disallowing the '-' character
	if (isNumeric(document.getElementById("precost-" + rownum).value, true)) {
		// this is a valid decimal number, reset colour to black in case of previous error
		clearPrecostError(rownum);
		clearExtendedcostError(rownum);
		return true;
	}
	else {
		setPrecostError(rownum);
		return false;
	}
}

function recalculateTable() {
	// iterates through the whole table and updates gst-inclusive costs and totals, on non-blank rows with no errors
	ordertotal = 0.0;
	for (i = 0; i < rowsvisible; i++) {
		if (document.getElementById("code-" + i).value != "" &&
			document.getElementById("qty-" + i).value != "" &&
			document.getElementById("precost-" + i).value != "" &&
			document.getElementById("codeerror-" + i).value == 0 &&
			document.getElementById("qtyerror-" + i).value == 0 &&
			document.getElementById("precosterror-" + i).value == 0 &&
			document.getElementById("extendedcosterror-" + i).value == 0) {
			// it's OK! Work out the GST-inclusive price
			var rowCostIncGst = parseFloat(document.getElementById("precost-" + i).value);
			if (document.getElementById("gst_select").value == "inc") {
				// gst is included, do nothing extra				
			}
			else {
				// add gst at 12.5%
				rowCostIncGst = (rowCostIncGst + rowCostIncGst * 0.125);
			}
			
			// work out the extended total
			rowExtendedCost = rowCostIncGst * parseInt(document.getElementById("qty-" + i).value);
			
			// increment the total order cost
			ordertotal += rowExtendedCost;
			
			if (rowCostIncGst.toFixed) {
				// display with rounding for javascript 1.5 browsers
				document.getElementById("cost-" + i).value = rowCostIncGst.toFixed(2);
				document.getElementById("postcost-" + i).value = rowCostIncGst.toFixed(2);
				document.getElementById("extended-cost-" + i).value = rowExtendedCost.toFixed(2);
			}
			else {
				// display them in the boxes (for javascript < 1.5 without rounding)
				document.getElementById("cost-" + i).value = rowCostIncGst;
				document.getElementById("postcost-" + i).value = rowCostIncGst;
				document.getElementById("extended-cost-" + i).value = rowExtendedCost;
			}
			
			
		}
			
	}
	
	// display the new total at the bottom, rounded to 2DP if possible
	if (ordertotal.toFixed) {
		document.getElementById("totalcost").value = ordertotal.toFixed(2); // for javascript >= 1,5
		document.getElementById("debit-drawer").value = ordertotal.toFixed(2);
	}
	else {
		document.getElementById("totalcost").value = ordertotal; // for javascript < 1.5 no rounding
		document.getElementById("debit-drawer").value = ordertotal;
	}
		
	return true;
}
	

function enteredRow(num) {
	// check if this is the last row, if so, add another row if we have one spare
	if (num == (rowsvisible -1)) {
		// this is the last row
		if (rowsvisible < numrows) {
			// we have rows to spare, add one
			document.getElementById("entryrow-" + (num+1)).style.display = "";
			rowsvisible++;
			return true;
		}
	}
	return true;
}

function clearRow(rownum) {
	// from the link in the table, clears the row fields
	document.getElementById("code-" + rownum).value = "";
	document.getElementById("descr-" + rownum).value = "";
	document.getElementById("qty-" + rownum).value = "";
	document.getElementById("precost-" + rownum).value = "";
	document.getElementById("cost-" + rownum).value = "";
	document.getElementById("extended-cost-" + rownum).value = "";
	
	clearCodeError(rownum);
	clearQtyError(rownum);
	clearPrecostError(rownum);
	clearExtendedcostError(rownum);
	
	// hide this row. count on them not doing this more than 100 times!
	document.getElementById("entryrow-" + rownum).style.display = "none";
	
	return true;
}

function checkBeforeSubmit() {
	// check that there's no outstanding errors, check the debit drawer field to make sure it's a number, and confirm
	if (!isNumeric(document.getElementById("debit-drawer").value, true)) {
		alert("Can't proceed because drawer debit amount isn't numeric! Fix it and try again");
		return false;
	}
	
	for (i = 0; i < rowsvisible; i++) {
		if (document.getElementById("code-" + i).value != "" && (
			document.getElementById("codeerror-" + i).value == 1 ||
			document.getElementById("qtyerror-" + i).value == 1 ||
			document.getElementById("precosterror-" + i).value == 1 ||
			document.getElementById("extendedcosterror-" + i).value == 1)) {
				alert("Can't proceed. There are errors on this form! Look for boxes with red text and correct them, then try again");
				return false;
		}
	}
	
	// stamp our approval on it to say that javascript has checked it
	document.getElementById("javascript-checked").value = "1";
	
	if(confirm("Are you sure you want to submit this order and debit the drawer balance by $" + document.getElementById("debit-drawer").value + "?")) {
		// submit the form
	}
	else {
		return false;
	}
}
	
function calculateBackwardsFromTotalRowCost(row) {
	document.getElementById("precost-" + row).value = ""; // clear this box in case qty has an error, so it knows to call us
	
	// this function simply sets up precost to be total / qty, then uses the recalculateTable() function to do the work
	
	if (!isNumeric(document.getElementById("extended-cost-" + row).value, true)) { 
		// we can't proceed, we're not a number
		setExtendedcostError(row);
		return;
	}
	else if (document.getElementById("qtyerror-" + row).value == "1") {
		// we can't proceed until qty is sorted out
		return;
	}
	else {
		clearExtendedcostError(row);
		clearPrecostError(row);
	}
		
	
	var rowCost = parseFloat(document.getElementById("extended-cost-" + row).value);
	var qty = parseInt(document.getElementById("qty-" + row).value)
	var preCost = rowCost / qty;
	
	document.getElementById("precost-" + row).value = preCost;
	
	recalculateTable();
}

function setCodeError(rownum) {
	document.getElementById("code-" + rownum).style.color = "red";
	document.getElementById("codeerror-" + rownum).value = "1";
	document.getElementById("descr-" + rownum).value = "INVALID PRODUCT CODE";
}

function clearCodeError(rownum) {
	document.getElementById("code-" + rownum).style.color = "black";
	document.getElementById("codeerror-" + rownum).value = "0";
}

function setPrecostError(rownum) {
	
}

function clearPrecostError(rownum) {
	document.getElementById("precosterror-" + rownum).value = "0";
	document.getElementById("precost-" + rownum).style.color = "black";
}

function setQtyError(rownum) {
	document.getElementById("qty-" + rownum).style.color = "red";
	document.getElementById("qtyerror-" + rownum).value = "1";
}

function clearQtyError(rownum) {
	document.getElementById("qtyerror-" + rownum).value = "0";
	document.getElementById("qty-" + rownum).style.color = "black";
	
	// see if we need to now trigger a function waiting on us to be OK
	if (document.getElementById("precost-" + rownum).value == "" && document.getElementById("extended-cost-" + rownum).value != "") {
		calculateBackwardsFromTotalRowCost(rownum);
	}
}

function setExtendedcostError(rownum) {
	document.getElementById("extended-cost-" + rownum).style.color = "red";
	document.getElementById("extendedcosterror-" + rownum).value = "1";
}

function clearExtendedcostError(rownum) {
	document.getElementById("extended-cost-" + rownum).style.color = "black";
	document.getElementById("extendedcosterror-" + rownum).value = "0";
}

-->
</script>


<?php printfinish(true); ?>
