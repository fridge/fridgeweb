<?php
require_once("includes.php");
printstart("Custom Database Queries", "Custom Database Queries");

// see if we have queries
if ($_POST[query]) {
	$query = stripslashes($_POST[query]); // override stripslashes
}
if ($_GET[fetchquery]) {
	$query = base64_decode(DBQueryOnce("select query_base64 from stored_queries where id='$_GET[fetchquery]'", "query_base64"));
}
if ($_GET[deletesaved]) {
	DBQuery("delete from stored_queries where id='$_GET[deletesaved]'");
}

// if they asked to save the query, do it
if ($_POST[savequery]) {
	$newqueryname = $_POST[queryname];
	$newquery = stripslashes($_POST[query]);
	
	// encode the query to make it safer to store in the database
	$encodedquery = base64_encode($newquery);
	
	// insert it into the stored queries table
	DBQuery("insert into stored_queries(name, query_base64) values('$newqueryname', '$encodedquery')"); 
}

print "<h2>Saved Queries</h2>";

$sqresult = DBQuery("select * from stored_queries order by name");

while ($row = pg_fetch_array($sqresult)) {
	print "<a href='query.php?fetchquery=$row[id]'>$row[name]</a> (<a href='query.php?deletesaved=$row[id]'>delete this query</a>)<br />";
}

?>
<h2>New Query</h2>
<form action="query.php" method="post">
<p>
<textarea rows="8" cols="120" name="query"><?php print $query; ?></textarea>
<br />
Save Query? <input type="checkbox" name="savequery" onclick="document.getElementById('querynamespan').style.display = '';" />&nbsp;&nbsp;
<span id="querynamespan" style="display: none;">Name: <input type="text" name="queryname" size="50" maxlength="100" /></span><br />
<br />
<input type="submit" value="Execute" />
</p>
</form>
<?php
if ($query) {
	print "<h2>Query Result</h2>";
	// execute the query
	$result = DBQuery($query);

	// tell them how many rows were affected
	print "<p style='text-align: center;'><b>Affected rows: ".pg_affected_rows($result)."</b></p>";
	
	// if there were actual results, get them
	if (pg_num_rows($result) > 0) {
		// get the column headers
		$colnames = array();
		for($colnum = 0; $colnum < pg_num_fields($result); $colnum++) {
			$colnames[] = pg_field_name($result, $colnum);
		}
		print "<table><tr>";
		foreach ($colnames as $colname) {
			print "<td class='tableheader'>$colname</td>";
		}
		print "</tr>";
		// print the data
		while ($row = pg_fetch_array($result)) {
			print "<tr>";
			foreach ($colnames as $colname) {
				print "<td>$row[$colname]</td>";
			}
			print "</tr>";
		}
		print "</table>";	
	}
}

printfinish(true);
?>
