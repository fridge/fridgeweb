<?php
include 'config.php';

// Import the postgres PHP module if needed
if (!extension_loaded('pgsql')) {
//	if (!dl('pgsql.so')) {
		die("PHP Postgres module not compiled in and not available as a library, I need pgsql.so!");
//	}
}

// connect to the fridge database (db configuration here!)
$link = pg_connect($DATABASE) or die("Couldn't open postgres connection. Check DB connection parameters in config.php");


// check that the user is authenticated (unless a cookie override has been requested
global $cookie_check_override, $no_session; // bring it into scope
if (!$no_session) {
	session_start();
	if (!$cookie_check_override) {
		if(!isset($_SESSION["fridge-admin-user"])) {
			// they have no set session cookie
			die("Sorry, you have no session cookie, or it has expired. Please <a href='index.php'>login</a> again, make sure cookies are on!");
		}
	}
}


// Issues an HTTP 302 redirect accepting a relative URL and attaching it to the absolute URL as calculated from PHP environmental 
function redirect($relativeurl) {
        // This function redirects the client's browser to the script specified in $relativeurl and terminates the script for secu

        $pathtoscripts = $_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']);
        $fullpath = "http://".$pathtoscripts."/".$relativeurl;
        header("Location: $fullpath");
        exit();
}


function DBQuery($sql) {
	// takes a sql query and returns the postgres result resource
	global $link;
	return pg_query($link, $sql);
}

function DBQueryOnce($sql, $requested_column) {
	// queries the DB and returns the value in the requested column on the first returned row
	global $link;
	$result = pg_query($link, $sql) or die("DB Query error");
	if (pg_num_rows($result) == 0) return null;
	$first_row_array = pg_fetch_array($result, 0, PGSQL_ASSOC);
	return htmlspecialchars($first_row_array[$requested_column], ENT_QUOTES);
}

function printstart($pagetitle, $pageheader) {
	// prints out the headers, filling in the title and head provided
	print '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<link rel="stylesheet" type="text/css" href="style.css" />
	<title>FridgeAdmin :: '.$pagetitle.'</title>
	</head>
	<body>
	<h1 class="pagetitle">'.$pageheader.'</h1>
	';
}

function printfinish($displaymenulink) {
	if ($displaymenulink) {
		print '<div id="footerlink"><a href="menu.php">&laquo; Return to main menu</a></div>';
		print '<div id="topreturnlink"><a href="menu.php">&laquo; Return to main menu</a></div>';
	}
	print '<div id="footer">FridgeAdmin 1.0 by <a href="mailto:neil@webbedfeet.net.nz">Neil Bertram</a> 2005<br /><a href="index.php?action=logoff"><b>Click here to log out</b></a></div></body></html>';
}

function creditUserBalance($username, $creditamount) {
	// creditamount is a + or - character followed by a number. can use as the arithmeticic operator here
	DBQuery("update users set balance=balance $creditamount where username='$username'");
	
	//write a log entry as well
	DBQuery("insert into user_credit_log (username, date_time, amount, transaction_type) values('$username', current_timestamp, '$creditamount', 'ADMIN')");
}

function filterArray($array) {
	// add htmlspecialchars to an array with single quotes escaped (ENT_QUOTES)
	if($array) {
//  if($array) added by Phil, 25-08-05 when the restock mysteriously stopped working.
		foreach ($array as $key => $value) {
			$array[$key] = htmlspecialchars($value, ENT_QUOTES);
		}
		return $array;
	}
}

?>
