<?php
require_once("includes.php");
require_once("database_functions.php");

if (!isset($_REQUEST[requesttype])) {
	// no request type was specified. it has to be, either through post of get
	die("user.php received no request type. It must be add or edit");
}

// if they submitted something, see what we're meant to do
if ($_POST) {
	if ($_POST[requesttype] == "add") {
		// they submitted something. check it
		$errors = checkUserDetailsBeforeAdd($_POST[username], $_POST[realname], $_POST[emailaddress], $_POST[password], $_POST[verifypassword], $_POST[initialbalance]);
	
		// If there's no errors, create the new user and redirect back to main menu
		if (count($errors) == 0) {
			createNewUser($_POST[username], $_POST[realname], $_POST[emailaddress], $_POST[password], $_POST[userclass], $_POST[usertype], $_POST[initialbalance], $_POST['usersponsor']);
			// kill the javascript window and reload the user list behind it
			?>
			<html><head><script type="text/javascript">window.close(); window.opener.location.replace("editusers.php");</script></head><body></body></html>
			<?php
			die(); //exit
		}
	}
	else {
		// this is an edit request. see if they entered valid values
		$errors = checkUserDetailsBeforeUpdate($_POST[username], $_POST[realname], $_POST[emailaddress]);
		
		// if there's no errors, commit the update
		if (count($errors) == 0) {
			updateUser($_POST[username], $_POST[realname], $_POST[emailaddress], $_POST[userclass], $_POST[usertype], $_POST['usersponsor']);
			// close javascript window and reload parent
			?>
			<html><head><script type="text/javascript">window.close(); window.opener.location.reload(true);</script></head><body></body></html>
			<?php
			die(); //exit
		}
	}
}

// title the page
if ($_REQUEST[requesttype] == "add") {
	printstart("Create new user", "Create new user");
}
else {
	printstart("Edit user", "Edit user");
}

// if there are errors to show, print them
if (count($errors) > 0) {
	print '<div class="errors"><b>There were errors with your request:<ul>';
	foreach ($errors as $error) {
		print "<li>$error</li>";
	}
	print "</ul></div>";
}

// construct all the default values (if request is a new request to edit, fetch from DB, otherwise they're what was previously submitted, if anything)
if ($_REQUEST[requesttype] == "edit" && !$_POST) {
	// this is a new edit request. Load details from the database
	$username = $_REQUEST[username];
	$realname = DBQueryOnce("select * from users where username='$username'", "real_name");
	$emailaddress = DBQueryOnce("select * from users where username='$username'", "email_address");
	// PHP has issues with the postgre boolean type, so we'll just use SQL to find out if they're an admin
	if (DBQueryOnce("select * from users where username='$username' and isadmin='true'", "username")) {
		$class_admin_selected = "selected=\"selected\"";
		$class_normal_selected = "";
	}
	else {
		$class_admin_selected = "";
		$class_normal_selected = "selected=\"selected\"";
	}
	if (DBQueryOnce("select * from users where username='$username' and isgrad='true'", "username")) {
		$type_grad_selected = "selected=\"selected\"";
		$type_undergrad_selected = "";
	} else {
		$type_grad_selected = "";
		$type_undergrad_selected = "selected=\"selected\"";
	}
	$current_sponsor = DBQueryOnce("select sponsor from users where username='$username'", 'sponsor');
}
else {
	// this is a second attempt to edit/add the user, or a new add request
	// everything below will be undefined (null) for a add user request.
	// some of it doesn't apply to failed edit attemps, but we don't display those
	// parts of the form, so it doesn't matter.
	$username = $_REQUEST[username];
	$realname = $_POST[realname];
	$emailaddress = $_POST[emailaddress];
	$password = $_POST[password];
	$verifypassword = $_POST[verifypassword];
	if ($_POST[userclass] == "admin") {
		$class_admin_selected = "selected=\"selected\"";
		$class_normal_selected = "";
	}
	else {
		$class_admin_selected = "";
		$class_normal_selected = "selected=\"selected\"";
	}
	$initialbalance = $_POST[intialbalance];
}
	

?>
<form action="user.php" method="post">
<input type="hidden" name="requesttype" value="<?php print $_REQUEST[requesttype]; ?>" />
<table>
<tr>
	<td class="tableheader">Username</td>
	<td>
	<?php if ($_REQUEST[requesttype] == "edit") { // don't allow them to change their username
		print "$username<input type=\"hidden\" name=\"username\" value=\"$username\" />";
	}
	else { ?>
		<input type="text" size="20" maxlength="20" name="username" value="<?php print $username; ?>" />
	<?php } ?>
	</td>
</tr>
<tr>
	<td class="tableheader">Real name</td>
	<td><input type="text" size="20" maxlength="60" name="realname" value="<?php print $realname; ?>" /></td>
</tr>
<tr>
	<td class="tableheader">Email address</td>
	<td><input type="text" size="20" maxlength="60" name="emailaddress" value="<?php print $emailaddress; ?>" /></td>
</tr>
<?php if ($_REQUEST[requesttype] == "add") { ?>
<tr>
	<td class="tableheader">Password</td>
	<td><input type="password" size="20" maxlength="20" name="password" value="<?php print $password; ?>" /></td>
</tr>
<tr>
	<td class="tableheader">Verify Password</td>
	<td><input type="password" size="20" maxlength="20" name="verifypassword" value="<?php print $verifypassword; ?>" /></td>
</tr>
<?php } ?>
<tr>
	<td class="tableheader">User class</td>
	<td><select name="userclass"><option <?php print $class_normal_selected; ?> value="normal">Normal user</option><option <?php print $class_admin_selected; ?> value="admin">Administrator user</option></select></td>
</tr>
<tr>
	<td class="tableheader">User type</td>
	<td><select name="usertype"><option <?php print $type_grad_selected; ?> value="grad">Grad</option><option <?php print $type_undergrad_selected; ?> value="undergrad">Undergrad</option></select></td>
</tr>
<tr>
	<td class="tableheader">Sponsor (required for undergrads)</td>
	<td><select name="usersponsor">
		<option value='undefined'>Graduate or unknown sponsor</option>
<?php
	$res = DBQuery("SELECT username,real_name FROM users WHERE isgrad = 'true' ORDER BY real_name");
	while ($row = pg_fetch_assoc($res)) {
		$selected =  ($_REQUEST[requesttype] == "edit" && $row['username'] == $current_sponsor) ? 'selected="selected"' : '';
		print '<option value="'.htmlspecialchars($row['username']).'" '.$selected.'>'.htmlspecialchars($row['real_name']).'</option>';
	}
?>
		</select></td>
</tr>

<?php if ($_REQUEST[requesttype] == "add") { ?>
<tr>
	<td class="tableheader">Initial Balance</td>
	<td>$&nbsp;<input type="text" size="10" maxlength="9" name="initialbalance" value="<?php if ($_POST) print $_POST[newinitialbalance]; else print "0"; ?>" /></td>
</tr>
<?php } ?>

<tr>
	<td></td><td><input type="submit" value="<?php if ($_REQUEST[requesttype] == "add") print "Add User"; else print "Save"; ?>" /></td>
</tr>
</table>
</form>

<?php printfinish(false); ?>

