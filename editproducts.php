<?php
require_once("includes.php");
require_once("database_functions.php");

// lists products with links to edit their details

printstart("Manage products", "Manage products");

// if they just saved the form, update everything that can be, and print errors about the rest
if ($_POST) {
	// print the start of the changes/errors table
	print "<table><tr><td class='tableheader'>Product details update result</td></tr>";
	
	// get an array of the current product state in the database to compare to
	$result = DBQuery("select * from product order by product_code");
	$product_array = array();
	
	while ($row = pg_fetch_array($result)) array_push($product_array, filterArray($row));

	// now loop through all products, checking which ones we may have updates for
	foreach ($product_array as $product_row_array) {
		// do we have a reference for this one on our submitted form?
		$curcode = $product_row_array[product_code];
		if ($_POST[$curcode]) {
			// we do, check if we have an update
			if (	$product_row_array[description] != htmlspecialchars($_POST["$curcode-descr"]) ||
				$product_row_array[category_id] != htmlspecialchars($_POST["$curcode-category"]) ||
				$product_row_array[cost] != $_POST["$curcode-cost"] ||
				$product_row_array[markup] != $_POST["$curcode-markup"] ||
				$product_row_array[stock_low_mark] != $_POST["$curcode-minstock"] ||
				$product_row_array[barcode] != $_POST["$curcode-barcode"] ||
				$product_row_array[enabled] != $_POST["$curcode-enabled"]) {
					
					//die($product_row_array[description] . " != " . $_POST["$curcode-descr"]);
					// we have a difference. insert them if they're valid, or report an error otherwise
					$updateresult = updateProductRowIfOK($curcode, $_POST["$curcode-descr"], $_POST["$curcode-category"], $_POST["$curcode-cost"], $_POST["$curcode-markup"], $_POST["$curcode-enabled"], $_POST["$curcode-minstock"]);
					if ($updateresult == "OK") {
						print "<tr class='ok'><td>$curcode details were updated successfully</td></tr>";
					}
					else {
						print "<tr class='err'><td>$curcode details could not be updated because $updateresult</td></tr>";
					}
			}
		}
	}
	
	print "</table>
	<table><tr><td class='tableheader'>Alter stock quantity result</td></tr>";
	
	// apply any stock alter requests, which must be inserted into the stock_alters table, and must be a DIFFERENCE, not a straight amount
	foreach ($_POST as $key => $value) {
		// is this a non-'+0' alter request?
		if (strstr($key, "-alterstock") && $value != "+0") {
			list($code, $null) = split("-alterstock", $key);
			// check for + or - character in first character, and a numeric value
			if ((substr($value, 0, 1) == "+" || substr($value, 0, 1) == '-') && is_numeric($value)) {
				// this is a valid alter string
				alterProductQuantity($code, $value);
				print "<tr class='ok'><td>$code quantity updated by $value successfully</td></tr>";
			}
			else {
				print "<tr class='err'><td>$code quantity could not be updated, &quot;$value&quot; is not a valid quantity difference</td></tr>";
			}
		}
	}
	print "</table>";
}


?>

<!-- javascript for popup edit -->
<script type="text/javascript">
<!--
function openWindow(url) {
	window.open(url, "", "status=0,toolbar=0,location=0,menubar=0,directories=0,resizable=0,scrollbars=1,height=400,width=600");
}
-->
</script>

<p style="text-align: center;"><a href="javascript: openWindow('addproduct.php');">Add new product</a></p>

<?php
	// sort out the current display options 
	if ($_REQUEST[display] == "all") {
		$sqlfilter = "";
	}
	else if ($_REQUEST[display] == "enabled") {
		$sqlfilter = "where enabled='true'";
	}
	else if ($_REQUEST[display] == "disabled") {
		$sqlfilter = "where enabled='false'";
	}
	else {
		die("no display mode");
	}
	
	if (!$_REQUEST[orderbycol]) $_REQUEST[orderbycol] = "product_code"; // default ordering by product code

?>
<form action="editproducts.php" method="post">

<p style="text-align:center;"><a href="editproducts.php?display=all">All Products</a> | <a href="editproducts.php?display=enabled">Enabled Products</a> | <a href="editproducts.php?display=disabled">Disabled products</a>
<input type="hidden" name="display" value="<?php print $_REQUEST[display] ?>" />
<input type="hidden" name="orderbycol" value="<?php print $_REQUEST[orderbycol] ?>" />
</p>
<table>
<tr>
	<td class="tableheader"><a href="editproducts.php?display=<?php print $_REQUEST[display]?>&amp;orderbycol=product_code">Code</a></td>
	<td class="tableheader"><a href="editproducts.php?display=<?php print $_REQUEST[display]?>&amp;orderbycol=description">Description</a></td>
	<td class="tableheader"><a href="editproducts.php?display=<?php print $_REQUEST[display]?>&amp;orderbycol=cost">Cost</a></td>
	<td class="tableheader"><a href="editproducts.php?display=<?php print $_REQUEST[display]?>&amp;orderbycol=markup">Markup</a></td>
	<td class="tableheader">Price</td>
	<td class="tableheader"><a href="editproducts.php?display=<?php print $_REQUEST[display]?>&amp;orderbycol=category_id">Category</a></td>
	<td class="tableheader"><a href="editproducts.php?display=<?php print $_REQUEST[display]?>&amp;orderbycol=in_stock">Stock</a></td>
	<td class="tableheader"><a href="editproducts.php?display=<?php print $_REQUEST[display]?>&amp;orderbycol=stock_low_mark">Min. Stock</a></td>
	<td class="tableheader">Alter stock</td>
	<td class="tableheader">Barcodes</td>
	<td class="tableheader"><a href="editproducts.php?display=<?php print $_REQUEST[display]?>&amp;orderbycol=enabled">Enabled?</a></td>
</tr>

<?php
// get categories
$result = DBQuery("select * from category order by category_id");
$category_array = array();

while ($rowarray = pg_fetch_array($result)) {
	$category_array[$rowarray[category_id]] = htmlspecialchars($rowarray[title], ENT_QUOTES);
}

// list users
$result = DBQuery("select * from product $sqlfilter order by $_REQUEST[orderbycol]");
//die("select * from product $sqlfilter order by $_REQUEST[orderbycol]");
$linecolour = 0;

while ($rowarray = filterArray(pg_fetch_array($result))) {
	if ($linecolour == 0) {
		$class = "list0";
		$linecolour++;
	}
	else {
		$class = "list1";
		$linecolour = 0;
	}
	
	print "<tr class=\"$class\">
	<td>$rowarray[product_code]<input type='hidden' name='$rowarray[product_code]' value='present' /></td>
	<td><input type='text' size='25' maxlength='60' name='$rowarray[product_code]-descr' value='$rowarray[description]' /></td>
	<td><input type='text' size='4' maxlength='9' name='$rowarray[product_code]-cost' value='$rowarray[cost]' /></td>
	<td><input type='text' size='4' maxlength='5' name='$rowarray[product_code]-markup' value='$rowarray[markup]' /></td>
	<td>". money_format("\$%.2n", ($rowarray[cost] + ($rowarray[cost] * ($rowarray[markup] / 100)))). "</td>
	<td><select name='$rowarray[product_code]-category'>";
	// print out category options, with the current option selected
	$currentcat = $rowarray[category_id];
	foreach ($category_array as $key => $value) {
		$key == $currentcat ? $selected = "selected=\"selected\"" : $selected = "";
		print "<option $selected value=\"$key\">$value</option>";
	}
	print "</select></td>
	<td>$rowarray[in_stock]</td>
	<td><input name='$rowarray[product_code]-minstock' type='text' size='2' maxlength='5' value='$rowarray[stock_low_mark]' /></td>
	<td><input name='$rowarray[product_code]-alterstock' type='text' size='3' maxlength='6' value='+0' /></td>
	<td>";
	// find the number of barcodes associates with this item
	$numbarcodes = DBQueryOnce("select count(barcode) as numbarcodes from barcode_product where product_code='$rowarray[product_code]'", "numbarcodes");
	if ($numbarcodes == 0) {
		$numbarcodes = "N/A";
	}
	print $numbarcodes;
	
	print"</td>
	<td><select name='$rowarray[product_code]-enabled'>";
	// print out enabled/disabled with current option selected
	if ($rowarray[enabled] == 't') {
		print "<option selected=\"selected\" value=\"t\">Enabled</option>
			<option value=\"f\">Disabled</option>";
	}
	else {
		print "<option value=\"t\">Enabled</option>
			<option selected=\"selected\" value=\"f\">Disabled</option>";
	}
	print "</select></td>
	</tr>";
}


?>
<tr><td></td><td></td><td></td><td colspan="2"><input type="submit" value="Save changes" /></td></tr>
</table>


</form>

<? printfinish(true); ?>
