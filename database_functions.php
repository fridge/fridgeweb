<?php
require_once("includes.php");

// this file specifies common database functions


function userExists($username) {
	if (DBQueryOnce("select * from users where username='$username'", "username")) {
		return true;
	}
	else {
		return false;
	}
}

function checkUserDetailsBeforeAdd($username, $realname, $email, $password, $verifypassword, $initialbalance) {
	// checks the provided arguments to ensure they're legal, returns an array of error messages
	$errors = array();
	
	if (strlen($username) == 0) {
		$errors[] = "Username field blank";
	}
	if ($check_duplicate_username && userExists($username)) {
		$errors[] = "Username already in use";
	}
	if (strlen($realname) == 0) {
		$errors[] = "Real name not specified";
	}
	if (strlen($email) == 0) {
		$errors[] = "Email address not specified";
	}
	
	// allow password to be blank as per old fridge system, so not checked
	
	if ($password != $verifypassword) {
		$errors[] = "Passwords do not match";
	}
	if (!is_numeric($initialbalance) || $initialbalance < 0) {
		$errors[] = "Invalid initial balance";
	}
	
	return $errors;
}

function checkUserDetailsBeforeUpdate($username, $realname, $email) {
	// checks the provided arguments to ensure they're legal, returns an array of error messages
	$errors = array();
	
	if (strlen($realname) == 0) {
		$errors[] = "Real name not specified";
	}
	if (strlen($email) == 0) {
		$errors[] = "Email address not specified";
	}
	
	return $errors;
}
	
function createNewUser($username, $realname, $email, $password, $class, $type, $initialbalance, $sponsor) {
	$class == "admin" ? $isadmin = "true" : $isadmin = "false";
	$isgrad = ($type == "grad") ? "true" : "false";

	// inset a new user into the DB
	$sql = "insert into users (username, real_name, email_address, password_md5, isadmin, isgrad, balance, sponsor) values('$username', '$realname', '$email', '".md5($password)."', '$isadmin', '$isgrad', '$initialbalance', '$sponsor')";
	DBQuery($sql);
}

function updateUser($username, $realname, $email, $class, $type, $sponsor) {
	$class == "admin" ? $isadmin = "true" : $isadmin = "false";
	$isgrad = ($type == "grad") ? "true" : "false";
	
	$sql = "update users set real_name='$realname', email_address='$email', isadmin='$isadmin', isgrad='$isgrad', sponsor='$sponsor' where username='$username'";
	DBQuery($sql);
}

function changePassword($username, $newpassword) {
	DBQuery("update users set password_md5='".md5($newpassword)."' where username='$username'");
}

function setProductsUpdated() {
	// raises a flag in the database that the products table has changed
	// this is so the client doesn't have to refresh the products list on every login (slow)
	DBQuery("update internal_variables set value='YES' where variable='products_updated'");
}

function addProduct($code, $description, $category_id, $initial_stock, $minimum_stock, $cost, $markup) {
	$sql = "insert into product(product_code, description, enabled, category_id, in_stock, stock_low_mark, cost, markup) values ('$code', '$description', 'true', '$category_id', '$initial_stock', '$minimum_stock', '$cost', '$markup')";	
	DBQuery($sql);
	// set database flat to say that products table has changed (causes refresh on FridgeClient)
	setProductsUpdated();
}

function checkIfProductExists($code) {
	if (DBQueryOnce("select product_code from product where product_code='$code'", "product_code") != null) {
		return true;
	}
	return false;
}

function updateProductRowIfOK($code, $descr, $category_id, $cost, $markup, $enabled, $minstock) {
	// check if the arguments are valid, if so, apply them to the db, if not, report why
	if (strlen($descr) == 0) return "description field was left blank";
	if (strlen($cost) == 0) return "cost field was left blank";
	if (strlen($markup) == 0) return "markup field was left blank";
	if (strlen($minstock) == 0) {die($minstock);return "minimum stock field was left blank";}
	if (!is_numeric($cost)) return "cost field was not numeric";
	if (!is_numeric($minstock)) return "minimum stock field not numeric";
	
	
	DBQuery("update product set description='$descr', category_id='$category_id', cost='$cost', markup='$markup', enabled='$enabled', stock_low_mark='$minstock' where product_code='$code'");
	// set database flat to say that products table has changed (causes refresh on FridgeClient)
	setProductsUpdated();
	return "OK";
}

function alterProductQuantity($code, $diff) {
	// update quantity by +/- amount (prechecked) and insert a row into stock_alters
	DBQuery("update product set in_stock=in_stock + '$diff' where product_code='$code'");
	DBQuery("insert into stock_alters(date_time, product_code, stock_difference) values(current_timestamp, '$code', '$diff')");
}

function getCurrentDrawerBalance() {
	return DBQueryOnce("select value from numerical_variables where variable='drawer_balance'", "value");
}

function debitDrawerBalance($amount) {
	DBQuery("update numerical_variables set value=value-$amount where variable='drawer_balance'");
}

function setDrawerBalance($newbalance) {
	DBQuery("update numerical_variables set value=$newbalance where variable='drawer_balance'");
}

function alterProductCost($code, $newcost, $newquantity) {
	// this function will weight the cost based on the new quantity
	// this ensures that old stock doesn't get over/under costed
	$sql = "update product set cost=(((cost * in_stock) + ($newcost * $newquantity)) / (in_stock + $newquantity)) where product_code='$code'";
	//die($sql);
	DBQuery($sql);
}

function transferCredit($amount, $from, $to) {
	// check all details (doesn't check credit limit yet)
	if (!is_numeric($amount) || $amount < 0) $errors[] = "Amount \$$amount is not numeric or is less than zero";
	if (!userExists($from)) $errors[] = "No such from user '$from'";
	if (!userExists($to)) $errors[] = "No such recipient user '$to'";
	if ($from == $to) $errors[] = "You can't have the same from and to users, this would cause a pkey exception! in the logs";

	// if no errors, do it inside a transaction
	if (count($errors) > 0) return $errors;

	DBQuery("BEGIN");
	DBQuery("update users set balance=balance-$amount where username='$from'");
	DBQuery("update users set balance=balance+$amount where username='$to'");
	DBQuery("insert into user_credit_log (username, date_time, amount, transaction_type, transfer_sent_to) values('$from', current_timestamp, '-$amount', 'XFER-OUT', '$to')");
	DBQuery("insert into user_credit_log (username, date_time, amount, transaction_type, transfer_received_from) values('$to', current_timestamp, '$amount', 'XFER-IN', '$from')");
	DBQUERY("COMMIT");
	return $errors; // empty array =ok
}
?>
