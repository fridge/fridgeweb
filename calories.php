<?php
require_once("includes.php");
require_once("database_functions.php");

printstart("Barcode management", "Barcodes");

// check for any actions to be performed
if ($_POST) {
	// we have a new mapping, make sure it's unique
	if (DBQueryOnce("select * from barcode_product where barcode='$_POST[barcode]'", "barcode")) {
		// it exists
		print "<h2 class='loginerror'>Error: that barcode is already mapped</h2>";
	}
	else {
		// let's add it
		DBQuery("insert into barcode_product (barcode, product_code) values('$_POST[barcode]', '$_POST[product]')");
		print "<h2 class='message'>New mapping added</h2>";
	}
}
if ($_GET[delete]) {
	// have to delete a mapping
	DBQuery("delete from barcode_product where barcode='$_GET[delete]'");
	print "<h2 class='message'>Mapping deleted</h2>";
}

?>
<p>Products can have more than one barcode (eg. different pies probably have different barcodes, but may fall under a single fridge code) but you can't have a barcode point to multiple products!</p>

<form action="barcodes.php" method="post">
<table>
<tr>
	<td style="text-align: center; font-weight: bold;" colspan="2">Add new mapping</td>
</tr>
<tr>
	<td class="tableheader">Barcode</td><td class="tableheader">Product</td>
</tr>
<tr>
	<td><input type="text" size="20" maxlength="20" name="barcode" /></td><td><select name="product"><?php
	$res = DBQuery("select * from product where enabled='t' order by product_code");
	while ($row = filterArray(pg_fetch_array($res))) {
		print "<option value='$row[product_code]'>$row[product_code]  -  $row[description]</option>";
	}
	?></select></td>
</tr>
<tr>
	<td></td><td><input type="submit" value="Create mapping" /></td>
</tr>
</table>
</form>

<table>
<tr>
	<td style="text-align: center; font-weight: bold;" colspan="3">Existing mappings</td>
</tr>
<tr>
	<td class="tableheader"><a href="barcodes.php?orderby=barcode">Barcode</a></td><td class="tableheader"><a href="barcodes.php?orderby=product_code">Product</a></td>
</tr>
<?php
	// get current mappings
	if ($_GET[orderby]) {
		$sql = "select product.product_code, product.description, barcode_product.barcode from barcode_product inner join product on barcode_product.product_code=product.product_code order by $_GET[orderby]";
	}
	else {
		$sql = "select product.product_code, product.description, barcode_product.barcode from barcode_product inner join product on barcode_product.product_code=product.product_code order by product_code";
	}
	$res = DBQuery($sql);
	while ($row = filterArray(pg_fetch_array($res))) {
		print "<tr><td>$row[barcode]</td><td><b>$row[product_code]</b> $row[description]</td><td><a href='barcodes.php?delete=$row[barcode]'>Delete this mapping</a></td></tr>";
	}
?>
</table>
<?php printfinish(true); ?>
