--Create table to store nonces used in interfridge authentication
create table nonces(snonce text not null primary key, cnonce text not null unique, created_at timestamp with time zone default now());

--Add column to users table to indicate that the user is not really a user but an account for a remote fridge 
alter table users add column isinterfridge boolean not null default false;

--Add columns to users table to store details for making interfridge connections
alter table users add column interfridge_password character varying(20) default null;
alter table users add column interfridge_endpoint text default null;

--New fridgeserver design used for interfridge
alter table users add column fridgeserver_endpoint text default null;
