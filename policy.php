<?php
//This file contains fridgeserver policy configuration for the local installation

/**
 * Decide whether to allow a particular transfer.
 * @return null to allow the transfer, or a string reason to deny the transfer.
 */
function policy_transfer($from_user, $to_user, $amount, $user_authenticated_balance, $is_interfridge, $is_grad) {
	if ($amount < 0) {
		return 'Transfer amount must be non-negative.';
	}
	//Check whether the user has enough balance to make the transfer (interfridge accounts are allowed to go negative)
	if ($amount > 0 && $user_authenticated_balance - $amount < 0 && !$is_interfridge) {
		return "Insufficient credit: user's balance is $user_authenticated_balance.";
	}
	return null;
}

/**
 * Decide whether to allow a particular local purchase.
 * @return null to allow the purchase, or a string reason to deny the purchase.
 */
function policy_purchase($user, $user_authenticated_balance, $is_interfridge, $is_grad) {
	//Check whether the user has a positive balance to make the purchase (interfridge accounts are allowed to go negative)
	if ($user_authenticated_balance < 0 && !$is_interfridge) {
		return "Insufficient credit: user's balance is $user_authenticated_balance.";
	}
	return null;
}

/**
 * Decide whether to allow a particular topup.
 * @return null to allow the topup, or a string reason to deny the topup.
 */
function policy_topup($user, $amount, $user_authenticated_balance, $is_interfridge, $is_grad) {
	return null;
}

/**
 * Decide whether to allow a particular remote purchase.
 * @return null to allow the purchase, or a string reason to deny the purchase.
 */
function policy_remote_purchase($purchase_fridge, $user, $user_authenticated_balance, $is_interfridge, $is_grad) {
	//Check whether the user has a positive balance to make the purchase (interfridge accounts are allowed to go negative)
	if ($user_authenticated_balance < 0 && !$is_interfridge) {
		return "Insufficient credit: user's balance is $user_authenticated_balance.";
	}
	return null;
}

/**
 * Decide whether to allow a particular remote topup.
 * @return null to allow the topup, or a string reason to deny the topup.
 */
function policy_remote_topup($purchase_fridge, $user, $amount, $user_authenticated_balance, $is_interfridge, $is_grad) {
	return null;
}
?>