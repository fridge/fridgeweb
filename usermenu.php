<?php
$cookie_check_override = true;
require_once("includes.php");


// get includes and authenticate user's cookie
if ($_SESSION["fridge-admin-user"]) {
	// they're ok
}
elseif ($_SESSION["fridge-normal-user"] && ($_GET[username] == $_SESSION["fridge-normal-user"])) {
	// they're ok
}
else {
	die("You're trying to be too tricky for your own good - you didn't log in as that!");
}

require_once("database_functions.php");

printstart("Main Menu", "What would you like to do?");


?>

<h3 class="section">What you can do!</h3>
<p><b>Your balance:</b> <?php print "\$" . DBQueryOnce("select balance from users where username='$_GET[username]';", "balance"); ?>
</p>
<p>
<a href="quickstock.php">See what's in stock now!</a><br />
<a href="userlog.php?username=<?php print $_GET[username]; ?>">Look at your transactions</a><br />
<a href="usertransfers.php?username=<?php print $_GET[username];?>">Look at your credits and transfers</a><br />
<a href="usertotal.php?username=<?php print $_GET[username];?>">See how much you've spent overall</a><br />
<a href="userbarcode.php?username=<?php print $_GET[username]; ?>">Print a barcode</a><br />
<a href="wishlist.php">Submit a new product you'd like the fridge to have, or vote on requests others have made</a>
</p>


<?php printfinish(false)?>

