<?php
$cookie_check_override = true;
require_once("includes.php");

if ($_SESSION["fridge-admin-user"]) {
	// they're ok
}
elseif ($_SESSION["fridge-normal-user"] && ($_GET[username] == $_SESSION["fridge-normal-user"])) {
	// they're ok
}
else {
	die("You're not authorised to view the logs for that user.");
}

// attempt to parse the stuff they gave us
if ($_GET[datefrom]) {
	$datefrom = strtotime($_GET[datefrom]);
	$dateto = strtotime($_GET[dateto]);
	
	if ($datefrom == -1 || $dateto == -1) {
		print "Could not parse one of the dates you specified. Try again!";
		printfinish("false");
		die();
	}
	else {
		// format them into SQL date strings
		$datefrom = date("Y-m-d H:m:s", $datefrom);
		$dateto = date("Y-m-d H:m:s", $dateto);
	}
}
elseif ($_GET[count]) {
	if (is_numeric($_GET[count]) && $_GET[count] > 0) {
		$count = $_GET[count];
	}
	else {
		print "Could not parse number of rows you wanted, was it numeric?";
		printfinish("false");
	}
}

header("Content-type: text/qif");
header("Content-Disposition: attachment; filename=\"{$_GET[username]}.qif\"");

// construct the sql for the transaction log excluding purchases
if ($count) {
	$sql = "select *, to_char(date_time, 'MM/DD/YYYY') AS date_string from user_credit_log where username='$_GET[username]' and transaction_type <> 'PURCHASE' order by date_time desc limit $count";
}
elseif ($datefrom) {
	$sql = "select *, to_char(date_time, 'MM/DD/YYYY') AS date_string from user_credit_log where (date_time < '$dateto') and (date_time > '$datefrom') and username='$_GET[username]' and transaction_type <> 'PURCHASE' order by date_time desc";
}
else {
	$onemonthago = date("Y-m-d H:m:s", strtotime("1 month ago"));
	$sql = "select *, to_char(date_time, 'MM/DD/YYYY') AS date_string from user_credit_log where username='$_GET[username]' and date_time > '$onemonthago' and transaction_type <> 'PURCHASE' order by date_time desc";
}

$result = DBQuery($sql);

echo "!Type:Bank\n"; #Cheque account (should this by Type:Cash?)

while ($row = pg_fetch_array($result)) {
	echo "D{$row['date_string']}\n";
	echo "T$row[amount]\n";
	if ($row['transaction_type'] == 'CREDIT') {
		echo "LFridge credit:Deposit\n";
	}
	else if ($row['transaction_type'] == 'XFER-OUT') {
		echo "LFridge debit:Debit transfer\n";
	}
	else if ($row['transaction_type'] == 'XFER-IN') {
		echo "LFridge credit:Credit transfer\n";
	}
	else {
		echo "L{$row['transaction_type']}\n";
	}
	if (!empty($row[transfer_received_from])) {
		echo "P{$row['transfer_received_from']}\n";
	}
	else if (!empty($row[transfer_sent_to])) {
		echo "P{$row['transfer_sent_to']}\n";
	}
	echo "#{$row['date_time']}\n"; #Unique identifier (KMyMoney extension)
	echo "^\n";
}

// construct the sql for the purchase log
if ($count) {
	$sql = "select *, to_char(date_time, 'MM/DD/YYYY') AS date_string from purchases where username='$_GET[username]' order by date_time desc limit $count";
}
elseif ($datefrom) {
	$sql = "select *, to_char(date_time, 'MM/DD/YYYY') AS date_string from purchases where (date_time < '$dateto') and (date_time > '$datefrom') and username='$_GET[username]' order by date_time desc";
}
else {
	$onemonthago = date("Y-m-d H:m:s", strtotime("1 month ago"));
	$sql = "select *, to_char(date_time, 'MM/DD/YYYY') AS date_string from purchases where username='$_GET[username]' and date_time > '$onemonthago' order by date_time desc";
}

$result = DBQuery($sql);

while ($row = pg_fetch_array($result)) {
	echo "D{$row['date_string']}\n";
	$amount = -$row['amount'];
	echo "T$amount\n";
	echo "LFridge debit:Purchase\n";
	$name = DBQueryOnce("select description from product where product_code='{$row['product_code']}'", 'description');
	echo "M{$name} ({$row['product_code']}) x {$row['purchase_quantity']}\n";
	echo "#{$row['date_time']}{$row['product_code']}\n"; #Unique identifier (KMyMoney extension)
	echo "^\n";
}

?>
