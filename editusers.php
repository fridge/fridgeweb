<?php
require_once("includes.php");
require_once("database_functions.php");

// lists users with links to edit their details

printstart("Manage users", "Manage users");

?>

<!-- javascript for popup edit -->
<script type="text/javascript">
<!--
function changePassword(username) {
	window.open("changepassword.php?username=" + username, "changepasswordwindow", "status=0,toolbar=0,location=0,menubar=0,directories=0,resizable=0,scrollbars=0,height=400,width=600");
}
	
function modifyBalance(username) {
	window.open("modifybalance.php?username=" + username, "modifybalancewindow", "status=0,toolbar=0,location=0,menubar=0,directories=0,resizable=0,scrollbars=0,height=400,width=600");
}

function editUser(username) {
	window.open("user.php?requesttype=edit&username=" + username, "edituserwindow", "status=0,toolbar=0,location=0,menubar=0,directories=0,resizable=0,scrollbars=0,height=400,width=600");
}

function addUser() {
	window.open("user.php?requesttype=add", "adduserwindow", "status=0,toolbar=0,location=0,menubar=0,directories=0,resizable=0,scrollbars=1,height=400,width=600");
}
-->
</script>

<p style="text-align: center;"><a href="javascript: addUser();">Add new user</a></p>

<table>
<tr>
	<td class="tableheader">Username</td>
	<td class="tableheader">Real name</td>
	<td class="tableheader">Email</td>
	<td class="tableheader">Balance</td>
	<td class="tableheader">User Level</td>
	<td class="tableheader">User type</td>
	<td class="tableheader">Sponsor</td>
</tr>

<?php
// list users
$result = DBQuery("select * from users order by username");

$linecolour = 0;

while ($rowarray = filterArray(pg_fetch_array($result))) {
	if ($linecolour == 0) {
		$class = "list0";
		$linecolour++;
	}
	else {
		$class = "list1";
		$linecolour = 0;
	}
	
	print "<tr class=\"$class\">
	<td>$rowarray[username]</td>
	<td>$rowarray[real_name]</td>
	<td>$rowarray[email_address]</td>
	<td>\$$rowarray[balance]</td>
	<td>";
	if ($rowarray[isadmin] == 't') {
		print "Administrator";
	}
	else {
		print "Normal User";
	}
	print "</td><td>";
	if ($rowarray['isgrad'] == 't') {
		print "Grad";
	} else {
		print "Undergrad";
	}
	print "</td><td>";
	if($rowarray['isgrad'] == 't') { // false&& removed by DJG 17/8/2k6
		print 'Grad'; 
	} else if (!$rowarray['sponsor'] || $rowarray['sponsor'] == 'undefined') {
		print "NONE!";
	} else {
		print htmlspecialchars(DBQueryOnce("SELECT real_name from users where username='".$rowarray['sponsor']."'", 'real_name'));
	}
	print "</td><td><a href=\"javascript: editUser('$rowarray[username]')\">Edit details</a></td>
	<td><a href=\"javascript: changePassword('$rowarray[username]')\">Change password</a></td>
	<td><a href=\"javascript: modifyBalance('$rowarray[username]')\">Modify balance</a></td>
	</tr>";
}
?>
</table>
<? printfinish(true); ?>
