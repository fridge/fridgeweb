<?php
#The globally-unique name of this fridge. This is used primarily for identifying the fridge in Interfridge transactions.
$LOCAL_FRIDGE_NAME = 'fridge1';

#PostgreSQL database connection string
$DATABASE = 'host=depot.mcs.vuw.ac.nz dbname=fridge user=fridge password=';

#Only admins are allowed to use fridgeweb
$ADMIN_ONLY = false;
?>