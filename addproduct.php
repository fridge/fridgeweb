<?php
require_once("includes.php");
require_once("database_functions.php");

printstart("Create new product", "Create new product");

// if we have input, check it
if ($_POST) {
	// they have submitted us a new product to add, verify it
	$errors = array();
	
	// convert the code to uppercase if reqd
	$_POST[code] = strtoupper($_POST[code]);
	
	// check for blank required fields
	if (strlen($_POST[code]) == 0) {
		$errors[] = "You must enter a product code";
	}
	if (strlen($_POST[description]) == 0) {
		$errors[] = "You must enter a description";
	}
	if (strlen($_POST[cost] == 0)) {
		$errors[] = "You must enter a cost";
	}
	if (strlen($_POST[markup] == 0)) {
		$errors[] = "You must enter a markup";
	}
	if (strlen($_POST[stock_level]) == 0) {
		$errors[] = "You must enter an initial stock level";
	}
	if (strlen($_POST[minstock]) == 0) {
		$errors[] = "You must enter a minimum stock level";
	}
	
	// check for invalid entries
	if (!is_numeric($_POST[cost])) {
		$errors[] = "The cost you entered is not a number";
	}
	if (!is_numeric($_POST[stock_level])) {
		$errors[] = "The stock level you entered is not a number";
	}
	if (!is_numeric($_POST[markup])) {
		$errors[] = "The markup you entered is not a number";
	}
	if (!is_numeric($_POST[minstock])) {
		$errors[] = "The minimum stock you entered is not a number";
	}
	
	// check for duplicate code
	if (checkIfProductExists($_POST[code])) {
		$errors[] = "That product code already exists";
	}
	
	
	// all checks done. if no errors, add it
	if (count($errors) == 0) {
		addProduct($_POST[code], $_POST[description], $_POST[category], $_POST[stock_level], $_POST[minstock], $_POST[cost], $_POST[markup]);
		// close javascript window
		print "<html><head><script type='text/javascript'>window.close(); window.opener.location.replace('editproducts.php?display=enabled');</script></head><body></body></html>";
		die();
	}
	else {
		// there's errors, report them
		print '<div class="errors"><b>There were errors with your request:<ul>';
		foreach ($errors as $error) {
			print "<li>$error</li>";
		}
		print "</ul></div>";
	}
		
}
else {
	// this is a new form. force the initial values for the boxes
	$_POST[price] = "0.00";
	$_POST[stock_level] = 0;
	$_POST[markup] = DBQueryOnce("select value from numerical_variables where variable='default_markup'", "value");
	$_POST[minstock] = floor(DBQueryOnce("select value from numerical_variables where variable='default_minimum_stock'", "value"));
}	


?>
<form action="addproduct.php" method="post">
<table>
<tr>
	<td class="tableheader">Code</td>
	<td><input type="text" size="5" maxlength="5" name="code" value="<?php print $_POST[code];?>" /></td>
</tr>
<tr>
	<td class="tableheader">Description</td>
	<td><input type="text" size="40" maxlength="60" name="description" value="<?php print $_POST[description];?>" /></td>
</tr>
<tr>
	<td class="tableheader">Category</td>
	<td><select name="category">
	<?php
		// list all product categories, with the currently chosen (if any) selected
		$result = DBQuery("select * from category order by category_id");

		$category_array = array();

		while ($rowarray = pg_fetch_array($result)) {
			$category_array[$rowarray[category_id]] = htmlspecialchars($rowarray[title], ENT_QUOTES);
		}
		
		foreach ($category_array as $catid => $cattitle) {
			$catid == $_POST[category] ? $selected = "selected=\"selected\"" : $selected = "";
			
			print "<option value='$catid' $selected>$cattitle</option>";
		}
	?>
	</select></td>
</tr>
<tr>
	<td class="tableheader">Initial stock</td>
	<td><input type="text" size="5" maxlength="5" name="stock_level" value="<?php print $_POST[stock_level];?>" /></td>
</tr>
<tr>
	<td class="tableheader">Cost $</td>
	<td><input type="text" size="8" maxlength="8" name="cost" value="<?php print $_POST[cost];?>" /></td>
</tr>
<tr>
	<td class="tableheader">Markup %</td>
	<td><input type="text" size="5" maxlength="5" name="markup" value="<?php print $_POST[markup];?>" /></td>
</tr>
<tr>
	<td class="tableheader">Minimum stock</td>
	<td><input type="text" size="5" maxlength="5" name="minstock" value="<?php print $_POST[minstock];?>" /></td>
</tr>
<tr>
	<td></td><td><input type="submit" value="Create" /></td>
</tr>
</table>
</form>

<?php printfinish(false); ?>
	
