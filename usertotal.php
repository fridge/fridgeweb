<?php
$cookie_check_override = true;
require_once("includes.php");

if ($_SESSION["fridge-admin-user"]) {
	// they're ok
}
elseif ($_SESSION["fridge-normal-user"] && ($_GET[username] == $_SESSION["fridge-normal-user"])) {
	// they're ok
}
else {
	die("You're not authorised to view results for that user.");
}

printstart("View totals for $_GET[username]", "View totals for $_GET[username]");


// construct the sql

    $sql = "select username, sum(amount) as total, (100*sum(amount)/(select sum(amount) from purchases))::numeric(9,2) as percentoffridge from purchases where username = '$_GET[username]' group by username order by total desc;";


$result = DBQuery($sql);

if ($row = pg_fetch_array($result)) {   
    
    print("<p>$row[username], your total amount spent is: $$row[total]<br />\n"); 
    print("That makes up $row[percentoffridge]% of the fridge's total amount spent.</p>\n");
	}

// admins can return to menu, users cannot

if ($_SESSION["fridge-admin-user"]) {
	printfinish(true);
}
else {
	printfinish(false);
}
?>
