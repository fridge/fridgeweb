<?php
require_once("includes.php");
require_once("database_functions.php");

printstart("Fridge Shopping List", "Fridge Shopping List");

if ($_GET) {
	// do they want to use default levels?
	if ($_GET[lowsource] == "default") {
		$sql = "select * from product where enabled='t' and in_stock < stock_low_mark order by product_code";
	}
	elseif ($_GET[lowsource] == "custom" && is_numeric($_GET[lowmark])) {
		$sql = "select * from product where enabled='t' and in_stock < '$_GET[lowmark]' order by product_code";
	}
	else die("No valid request, make sure you specify the lowmark as a number if you use custom lowmarks!");
}
else {
	die("no request");
}
?>
<table class="shoppinglisttable">
<tr>
	<td class="tableheader">Code</td>
	<td class="tableheader">Description</td>
	<td class="tableheader">Current Stock</td>
	<td class="tableheader">Min Stock</td>
	<td class="tableheader">Cost</td>
	<td class="tableheader">Price</td>
</tr>
<?php
// print table, sorted by fridgecode
$result = DBQuery($sql);
while ($rowarray = filterArray(pg_fetch_array($result))) {
	print "<tr><td>$rowarray[product_code]</td><td>$rowarray[description]</td><td>$rowarray[in_stock]</td><td>$rowarray[stock_low_mark]</td><td>\$$rowarray[cost]</td><td>".money_format("\$%.2n", ($rowarray[cost] + ($rowarray[cost] * ($rowarray[markup] / 100))))."</tr>";
}
print "</table>";

printfinish("true");
?>
	

