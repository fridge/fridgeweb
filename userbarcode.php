<?php
$cookie_check_override = true;
require_once("includes.php");

if ($_SESSION["fridge-admin-user"]) {
	// they're ok
}
elseif ($_SESSION["fridge-normal-user"] && ($_GET[username] == $_SESSION["fridge-normal-user"])) {
	// they're ok
}
else {
	die("You're not authorised to print a code for that user.");
}

printstart("View barcode for $_GET[username]", "View barcode for $_GET[username]");


// construct the sql

$sql = "select upper(username) as username,
        upper(substring(password_md5,0,6)) as hash 
        from users where username='$_GET[username]' ";

$result = DBQuery($sql);

if ($row = pg_fetch_array($result)) {   
    $barcodestring = "0" . $row[username] . $row[hash];
    print("<p>Your barcode is:<br />"); // string is: ". $barcodestring . "<br />\n");
    print("<img width=320 src=\"http://ida.20after4.net/servlet/com.idautomation.linear.IDAutomationServlet?FORMAT=GIF&BARCODE=".$barcodestring."&BAR_HEIGHT=2&CODE_TYPE=CODE39&X=0.06&CHECK_CHAR=N&CHECK_CHARINTEXT=N&CODE128_SET=0&ST=N\"> <br />\n");
    print($_GET[username]."</p>\n");
	}

// admins can return to menu, users cannot

if ($_SESSION["fridge-admin-user"]) {
	printfinish(true);
}
else {
	printfinish(false);
}
?>
