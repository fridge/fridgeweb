<?php 
// instruct the cookie checker that we don't care that there's no cookie yet
$cookie_check_override = true;
require_once("includes.php");

// check if the login failed or not

if ($_GET[action] == "logoff") {
	// detroy their session
	session_unset();
	session_destroy();
}

if ($_POST) {
	if ($isadminuser = DBQueryOnce("select * from users where username='" . pg_escape_string($_POST[username]) . "' and password_md5='".md5($_POST[password])."'", "isadmin")) {
		// login succeded, feed them a session id if they're admin, or just redirect them to their log otherwise
		if ($isadminuser == 't') {
			$_SESSION["fridge-admin-user"] = $_POST[username];
			redirect("menu.php");
		}
		else if (!$ADMIN_ONLY) {
			// they're a normal user
			$_SESSION["fridge-normal-user"] = $_POST[username];
			redirect("usermenu.php?username=$_POST[username]");
		}
	}
	else {
		$loginfailed = true;
		session_destroy(); // clears any set session
	}
}
else {
	$loginerror = false;
}

// print out the header
printstart("Please Login", "Welcome to FridgeAdmin");

if ($loginfailed) {
	print '<h2 class="loginerror">Your login failed, please try again</h2';
}
else {
	print '<h2>Please login</h2>';
}
?>

<form action="index.php" method="post">
<table class="logintable">
	<tr>
		<td class="tableheader"><label for="usernamebox">Username</label></td>
		<td class="tableheader"><label for="passwordbox">Password</label></td>
	</tr>
	<tr>
		<td><input type="text" size="10" name="username" id="usernamebox" /></td>
		<td><input type="password" size="10" name="password" id="passwordbox" /></td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" value="Login" /></td>
	</tr>
</table>
</form>
		

<?php printfinish(false); ?>

