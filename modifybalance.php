<?php
require_once("includes.php");
require_once("database_functions.php");

printstart("Modify Balance", "Modify balance for " . $_REQUEST[username]);

if ($_POST) {
	// check that there is a + or - in the first character of the amount
	$firstcharacter = substr($_POST[baldiff], 0, 1);
	if (!($firstcharacter == '+' || $firstcharacter == '-')) {
		// didn't start with + or -
		print "<h2 class='loginerror'>First character in the box <b>must be a + or -</h2>";
	}
	else {
		// check if it's numeric
		if (is_numeric($_POST[baldiff])) {
			// all good. commit the change
			creditUserBalance($_POST[username], $_POST[baldiff]);
			print '<html><head><script type="text/javascript">window.close(); window.opener.location.reload(true);</script></head><body></body></html>';
			die();
		}
		else {
			print "<h2 class='loginerror'>That isn't numeric. Make sure you enter +/- and a number, not spaces or any other characters!</h2>";
		}
	}
}
?>

<p style="text-align: center;">Please enter the <b>difference</b> in balance, not the actual new balance.<br />
Example: +50 or -24</p>

<form action="modifybalance.php" method="post">
<input type="hidden" name="username" value="<?php print $_REQUEST[username]; ?>" />
<table>
<tr>
	<td class="tableheader">Difference in balance (+/-):</td>
	<td><input type="text" name="baldiff" size="7" maxlength="7" value="+0" /></td>
</tr>
<tr>
	<td colspan="2"><input type="submit" value="Apply" /></td>
</tr>
</table>
</form>

<?php printfinish(false); ?>
