<?php
require_once("includes.php");
require_once("database_functions.php");

printstart("Transfer credit", "Transfer credit");

// check for input
if ($_POST) {
	// got some, attempt to transfer it
	$errors = transferCredit($_POST[amount], $_POST[fromuser], $_POST[touser]);
	if (count($errors) == 0) {
		print "<h2 class='message'>Funds tranferred OK</h2>";
	}
	else {
		print "<h2 class='loginerror'>Funds weren't tranferred because:</h2>";
 		print "<ul>";
		foreach ($errors as $error) {
			print "<li>$error</li>";
		}	
		print "</ul>";
	}
}

?>
<form action="transfercredit.php" method="post">
<table>
<tr>
	<td class="tableheader">Amount</td>
	<td class="tableheader">From usercode</td>
	<td class="tableheader">To usercode</td>
</tr>
<tr>
	<td><input type="text" size="5" name="amount" id="amount" /></td>
	<td><input type="text" size="10" name="fromuser" /></td>
	<td><input type="text" size="10" name="touser" value="<?php print $_POST[touser];?>" /></td>
</tr>
<tr>
	<td colspan="3"><input type="submit" value="Transfer" /></td>
</tr>
</table>
</form>
<script type="text/javascript">
// put focus in first form field on load
document.getElementById("amount").focus();
</script>

<?php printfinish(true); ?>