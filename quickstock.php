<?php
$cookie_check_override = true;
require_once("includes.php");


$result = DBQuery("select * from product where enabled='t' order by product_code");

if ($_GET["mode"] == "rss") {
	header("Content-type: application/rss+xml");
	// Compose the listing
	while ($row = pg_fetch_array($result)) {
		$rssoutput.= $row["product_code"]." (".htmlspecialchars($row["description"]).") has ".$row["in_stock"]." in stock at a price of ";
		// calculate the price from cost + markup
		$cost = money_format("\$%.2n", ($row["cost"] + ($row["cost"] * ($row["markup"] / 100))));
		$rssoutput.= $cost." each.<br />\n";
	}
	print '<?xml version="1.0" ?>'.chr(10);
	?>
<rss version="2.0">

<channel>

<title>FridgeAdmin Stock Levels</title>
<description>This feed provides a complete stock listing for anyone who cares</description>
<link>http://playground.mcs.vuw.ac.nz/~fridge/quickstock.php</link>

<item>
<title>Fridge Stock Levels</title>
<description><?php print $rssoutput; ?></description>
<link>http://playground.mcs.vuw.ac.nz/~fridge/quickstock.php</link>
</item>
</channel>

</rss>
<?php
	exit();
}
else if ($_GET["mode"] == "tsv") {
	header("Content-type: text/plain");
	// Compose the listing
	while ($row = pg_fetch_array($result)) {
		// calculate the price from cost + markup
		$cost = money_format("%.2n", ($row["cost"] + ($row["cost"] * ($row["markup"] / 100))));
		echo $row["product_code"] . "\t" . $row["description"] . "\t" . $row["in_stock"] . "\t" . $cost . "\n";
	}
	exit();
}

printstart("Stock level list", "Stock level list");

print '<p>This listing is available as an rss feed at <a href="http://playground.mcs.vuw.ac.nz/~fridge/quickstock.php?mode=rss">http://playground.mcs.vuw.ac.nz/~fridge/quickstock.php?mode=rss</a> but may not look too pretty!</p>';

print '<table><tr><td class="tableheader">Product Code</td><td class="tableheader">Product Name</td><td class="tableheader">In stock</td><td class="tableheader">Price each</td></tr>';

$list = 0;
while ($row = pg_fetch_array($result)) {
	$list == 0 ? $list = 1 : $list = 0;
	print "<tr class='list$list'><td>".$row["product_code"]."</td><td>".htmlspecialchars($row["description"])."</td><td>".$row["in_stock"]."</td><td>";
	// calculate the price from cost + markup
	$cost = money_format("\$%.2n", ($row["cost"] + ($row["cost"] * ($row["markup"] / 100))));
	print $cost."</td></tr>";
}

print "</table>";

// admins can return to menu, users cannot

if ($_SESSION["fridge-admin-user"]) {
        printfinish(true);
}
else {
        printfinish(false);
}
?>



