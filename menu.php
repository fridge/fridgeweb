<?php
require_once("includes.php"); // get includes and authenticate user's cookie
require_once("database_functions.php");

// if they want to debit/set the drawer balance, do it
if ($_POST[debitamount]) {
	// they want to debit the drawer
	if (is_numeric($_POST[debitamount])) {
		// do it
		debitDrawerBalance($_POST[debitamount]);
	}
	else {
		die("Debit amount not a number. Press back and try again");
	}
}
elseif ($_POST[newdrawerbalance]) {
	// they want to set it
	if (is_numeric($_POST[newdrawerbalance])) {
		// do it
		setDrawerBalance($_POST[newdrawerbalance]);
	}
	else {
		die("New drawer balance not a number. Press back and try again");
	}
}


printstart("Main Menu", "What would you like to do?");

print "<h2 class='message'>$_GET[message]</h2>";

?>
<script type="text/javascript">
<!--
function openWindow(url) {
	window.open(url, "", "status=0,toolbar=0,location=0,menubar=0,directories=0,resizable=0,scrollbars=1,height=400,width=600");
}
-->
</script>

<h3 class="section">Drawer balance</h3>

<p>Below is the balance that should be in the drawer. You can debit it after a shopping trip, or set it to what it actually is.<br />
If this is a user account withdrawl, make sure to debit the user's account balance in the user management section too!</p>

<p>The current drawer balance should be $<?php print getCurrentDrawerBalance(); ?></p>
<p><b>You can debit or set the drawer balance below</b></p>
<form action="menu.php" method="post">
<p><input type="text" size="5" maxlength="6" name="debitamount" />&nbsp;<input type="submit" value="Debit" /></p></form>
<form action="menu.php" method="post">
<p><input type="text" size="5" maxlength="6" name="newdrawerbalance" />&nbsp;<input type="submit" value="Set" /></p></form>

<h3 class="section">User management</h3>

<form action="userlog.php" method="get">View log for user: <input type="text" size="10" name="username" />&nbsp;<input type="submit" value="Show" /></form>
<form action="usertransfers.php" method="get">View transfers for user: <input type="text" size="10" name="username" />&nbsp;<input type="submit" value="Show" /></form>
<form action="userbarcode.php" method="get">View barcode for user: <input type="text" size="10" name="username" />&nbsp;<input type="submit" value="Show" /></form>

<p>
<a href="javascript: openWindow('user.php?requesttype=add');">Create new user</a><br />
<a href="editusers.php">Manage current users</a><br />
<a href="transfercredit.php">Transfer credit between users</a>
</p>

<h3 class="section">Product management</h3>
<p>
<a href="javascript: openWindow('addproduct.php');">Create new product</a><br />
Manage current products: <a href="editproducts.php?display=all">All</a> | <a href="editproducts.php?display=enabled">Enabled</a> | <a href="editproducts.php?display=disabled">Disabled</a><br />
<a href="restock.php">Restock fridge</a><br />
<a href="barcodes.php">Barcode management</a>
</p>

<h3 class="section">Shopping list</h3>

<form action="shoppinglist.php" method="get">
<p>Print shopping list of all enabled products with stock level below
	<select name="lowsource" id="lowsource">
		<option selected="selected" value="default">Individual levels</option>
		<option value="custom" onclick="document.getElementById('lowmark').value=10;">Custom level:</option>
	</select>
	<input type="text" size="4" name="lowmark" id="lowmark" /> <input type="submit" value="List" /></p>
</form>
<p>View the wishlist <a href="wishlist.php">here</a></p>

<h3 class="section">Database management</h3>
<p>
<a href="vacuumdb.php">VACUUM the database</a><br />
<a href="query.php">Perform a custom query</a>
</p>

<h3 class="section">Internal variables</h3>
<p>Here you can edit the numerical variables in the database directly. These are only checked by postgres, so watch for errors!</p>
<form action="menu.php" method="post">
<table>
<tr><td class="tableheader">Variable</td><td class="tableheader">Value</td></tr>
<?php
// check for changes
if ($_POST["numvar-change"]) {
	foreach ($_POST as $key => $value) {
		if (strstr($key, "var-")) {
			// save it
			list($null, $var) = split("var-", $key);
			//die("update numerical_variables set value='$value' where variable='$var'");
			DBQuery("update numerical_variables set value='$value' where variable='$var'");
		}
	}
}

$res = DBQuery("select * from numerical_variables order by variable");
while ($rowarr = filterArray(pg_fetch_array($res))) {

	print "<tr><td>$rowarr[variable]</td><td><input type='text' value='$rowarr[value]' name='var-$rowarr[variable]' /></td></tr>";

}
?>
<tr><td></td><td><input type="hidden" name="numvar-change" value="1" /><input type="submit" value="Save" /></td></tr>
</table>
</form>

<?php printfinish(false)?>

